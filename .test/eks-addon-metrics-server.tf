################################################################################
# Modules
################################################################################

module "eks_addon_metrics_server" {
  source  = "../eks/addon/metrics-server"
  vpc     = local.vpc
  owner   = local.owner
  company = local.company
  cluster = module.eks_node_group.cluster
}

################################################################################

