################################################################################
# Modules
################################################################################

module "eks_addon_kube_proxy" {
  source  = "../eks/addon/kube-proxy"
  vpc     = local.vpc
  owner   = local.owner
  company = local.company
  cluster = module.eks_node_group.cluster
}

################################################################################

