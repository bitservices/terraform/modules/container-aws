################################################################################
# Modules
################################################################################

module "eks_access" {
  source    = "../eks/access"
  vpc       = local.vpc
  owner     = local.owner
  company   = local.company
  cluster   = module.eks_cluster.name
  principal = "arn:aws:iam::012345678901:user/username"
}

################################################################################

