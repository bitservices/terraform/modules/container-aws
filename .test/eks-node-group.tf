################################################################################
# Modules
################################################################################

module "eks_node_group" {
  source      = "../eks/node-group"
  vpc         = local.vpc
  class       = format("default-%s", local.zone_b)
  owner       = local.owner
  company     = local.company
  cluster     = module.eks_cluster.name
  release     = module.eks_cluster.release
  subnet_zone = local.zone_b
}

################################################################################
