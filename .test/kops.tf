################################################################################
# Locals
################################################################################

locals {
  account            = "sandpit"
  kops_name          = format("%s.k8s.local", local.vpc)
  kops_vpc_id        = "vpc-01234567890abcdef"
  kops_subnet_set    = "default"
  kops_sums_table    = format("kops-sums-%s-%s-%s", local.vpc, local.region, local.account)
  kops_data_bucket   = format("kops-data-%s-%s-%s", local.vpc, local.region, local.account)
  kops_subnet_1_id   = "subnet-01234567890abcde0"
  kops_subnet_2_id   = "subnet-01234567890abcde1"
  kops_subnet_3_id   = "subnet-01234567890abcde2"
  kops_subnet_tier   = "private"
  kops_state_bucket  = format("kops-state-%s-%s-%s", local.vpc, local.region, local.account)
  kops_subnet_1_name = format("%s - %s - %s - %s", local.vpc, local.kops_subnet_set, local.kops_subnet_tier, local.zone_a)
  kops_subnet_2_name = format("%s - %s - %s - %s", local.vpc, local.kops_subnet_set, local.kops_subnet_tier, local.zone_b)
  kops_subnet_3_name = format("%s - %s - %s - %s", local.vpc, local.kops_subnet_set, local.kops_subnet_tier, local.zone_c)
  kops_vpc_ipv4_cidr = "192.168.188.0/23"
}

################################################################################
# Resources
################################################################################

resource "tls_private_key" "kops" {
  algorithm = "ED25519"
}

################################################################################
