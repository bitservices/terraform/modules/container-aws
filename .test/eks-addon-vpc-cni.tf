################################################################################
# Modules
################################################################################

module "eks_addon_vpc_cni" {
  source                       = "../eks/addon/vpc-cni"
  vpc                          = local.vpc
  owner                        = local.owner
  company                      = local.company
  cluster                      = module.eks_node_group.cluster
  iam_role_kubernetes_oidc_arn = module.eks_cluster.iam_openid_connect_provider_arn
  iam_role_kubernetes_oidc_url = module.eks_cluster.iam_openid_connect_provider_url
}

################################################################################

