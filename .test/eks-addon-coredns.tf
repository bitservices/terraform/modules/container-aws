################################################################################
# Modules
################################################################################

module "eks_addon_coredns" {
  source  = "../eks/addon/coredns"
  vpc     = local.vpc
  owner   = local.owner
  company = local.company
  cluster = module.eks_node_group.cluster
}

################################################################################

