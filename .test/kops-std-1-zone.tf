################################################################################
# Modules
################################################################################

module "kops_std_1_zone" {
  source            = "../kops/std-1-zone"
  key               = tls_private_key.kops.private_key_pem
  name              = local.kops_name
  owner             = local.owner
  company           = local.company
  vpc               = local.vpc
  vpc_id            = local.kops_vpc_id
  vpc_ipv4_cidr     = local.kops_vpc_ipv4_cidr
  subnet_id         = local.kops_subnet_1_id
  subnet_name       = local.kops_subnet_1_name
  subnet_zone       = local.zone_a
  sums_table_name   = local.kops_sums_table
  data_bucket_name  = local.kops_data_bucket
  state_bucket_name = local.kops_state_bucket
}

################################################################################
