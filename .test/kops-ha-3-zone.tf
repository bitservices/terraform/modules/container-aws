################################################################################
# Modules
################################################################################

module "kops_ha_3_zone" {
  source            = "../kops/ha-3-zone"
  key               = tls_private_key.kops.private_key_pem
  name              = local.kops_name
  owner             = local.owner
  company           = local.company
  vpc               = local.vpc
  vpc_id            = local.kops_vpc_id
  vpc_ipv4_cidr     = local.kops_vpc_ipv4_cidr
  subnet_1_id       = local.kops_subnet_1_id
  subnet_1_name     = local.kops_subnet_1_name
  subnet_1_zone     = local.zone_a
  subnet_2_id       = local.kops_subnet_2_id
  subnet_2_name     = local.kops_subnet_2_name
  subnet_2_zone     = local.zone_b
  subnet_3_id       = local.kops_subnet_3_id
  subnet_3_name     = local.kops_subnet_3_name
  subnet_3_zone     = local.zone_c
  sums_table_name   = local.kops_sums_table
  data_bucket_name  = local.kops_data_bucket
  state_bucket_name = local.kops_state_bucket
}

################################################################################
