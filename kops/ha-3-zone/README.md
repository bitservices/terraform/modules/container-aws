<!----------------------------------------------------------------------------->

# kops/ha-3-zone

#### Produces a [KOPS] template for a [Kubernetes] cluster based on a highly available 3 availability zone architecture that will run on [AWS]

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/container/aws//kops/ha-3-zone`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "owner"   { default = "terraform@bitservices.io" }
variable "account" { default = "sandpit"                  }
variable "company" { default = "BITServices Ltd"          }

module "my_vpc_core" {
  source          = "gitlab.com/bitservices/network/aws//vpc/core"
  name            = "sandpit01"
  owner           = var.owner
  company         = var.company
  ipv4_cidr       = "192.168.188.0/23"
  kubernetes_type = "kops"
}

module "my_subnets_3_tier_legacy_dev" {
  source                    =   "gitlab.com/bitservices/network/aws//subnets/3-tier-legacy-dev"
  vpc                       =   module.my_vpc_core.name
  vpc_id                    =   module.my_vpc_core.id
  owner                     =   var.owner
  company                   =   var.company
  kubernetes_cluster        =   module.my_vpc_core.kubernetes_cluster
  availability_zones        = [ "a", "b"                                                                                       ]
  subnet_public_ipv4_cidrs  = [ cidrsubnet(module.my_vpc_core.ipv4_cidr, 3, 6), cidrsubnet(module.my_vpc_core.ipv4_cidr, 3, 7) ]
  subnet_backend_ipv4_cidrs = [ cidrsubnet(module.my_vpc_core.ipv4_cidr, 3, 4), cidrsubnet(module.my_vpc_core.ipv4_cidr, 3, 5) ]
  subnet_private_ipv4_cidrs = [ cidrsubnet(module.my_vpc_core.ipv4_cidr, 2, 0), cidrsubnet(module.my_vpc_core.ipv4_cidr, 2, 1) ]
}

module "my_state_s3_bucket" {
  source  = "gitlab.com/bitservices/storage/aws//s3/bucket"
  owner   = var.owner
  class   = format("kops-state-%s", module.my_vpc_core.name)
  account = var.account
  company = var.company
}

module "my_data_s3_bucket" {
  source  = "gitlab.com/bitservices/storage/aws//s3/bucket"
  owner   = var.owner
  class   = format("kops-data-%s", module.my_vpc_core.name)
  account = var.account
  company = var.company
}

module "my_sums_dynamodb_table" {
  source  = "gitlab.com/bitservices/database/aws//dynamodb/autoscaling-table"
  class   = format("kops-sums-%s", module.my_vpc_core.name)
  owner   = var.owner
  account = var.account
  company = var.company
}

module "my_kops_ha_3_zone" {
  source            = "gitlab.com/bitservices/container/aws//kops/ha-3-zone"
  key               = file("~/.ssh/id_rsa.pub")
  name              = module.my_vpc_core.kubernetes_cluster
  owner             = var.owner
  company           = var.company
  vpc               = module.my_vpc_core.name
  vpc_id            = module.my_vpc_core.id
  vpc_ipv4_cidr     = module.my_vpc_core.cidr
  subnet_1_id       = module.my_subnets_3_tier_legacy_dev.subnet_private_ids[0]
  subnet_1_name     = module.my_subnets_3_tier_legacy_dev.subnet_private_names[0]
  subnet_1_zone     = module.my_subnets_3_tier_legacy_dev.subnet_private_zones[0]
  subnet_2_id       = module.my_subnets_3_tier_legacy_dev.subnet_private_ids[1]
  subnet_2_name     = module.my_subnets_3_tier_legacy_dev.subnet_private_names[1]
  subnet_2_zone     = module.my_subnets_3_tier_legacy_dev.subnet_private_zones[1]
  subnet_3_id       = module.my_subnets_3_tier_legacy_dev.subnet_private_ids[2]
  subnet_3_name     = module.my_subnets_3_tier_legacy_dev.subnet_private_names[2]
  subnet_3_zone     = module.my_subnets_3_tier_legacy_dev.subnet_private_zones[2]
  sums_table_name   = module.my_sums_dynamodb_table.name
  data_bucket_name  = module.my_data_s3_bucket.name
  state_bucket_name = module.my_state_s3_bucket.name
}
```

<!----------------------------------------------------------------------------->

[AWS]:        https://aws.amazon.com/
[KOPS]:       https://kops.sigs.k8s.io/
[Kubernetes]: https://kubernetes.io/

<!----------------------------------------------------------------------------->
