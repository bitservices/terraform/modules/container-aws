################################################################################
# Required Variables
################################################################################

variable "key" {
  type        = string
  description = "The SSH public key for the EC2 instances that make up this Kubernetes cluster."
}

variable "name" {
  type        = string
  description = "Name of the Kubernetes cluster this template will be created for."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################

variable "sums_table_name" {
  type        = string
  description = "The name of the DynamoDB table that holds the MD5 sums of exported KOPS data."
}

variable "data_bucket_name" {
  type        = string
  description = "The name of the S3 bucket that will be used to store KOPS data."
}

variable "state_bucket_name" {
  type        = string
  description = "The name of the S3 bucket that will be used to store KOPS state."
}

################################################################################

variable "vpc" {
  type        = string
  description = "The name of the VPC to which this asset belongs."
}

variable "vpc_id" {
  type        = string
  description = "The ID of the VPC to which this asset belongs."
}

variable "vpc_ipv4_cidr" {
  type        = string
  description = "The IPv4 CIDR of the VPC to which this asset belongs."
}

################################################################################

variable "subnet_1_id" {
  type        = string
  description = "The ID of the subnet for the 1st availability zone."
}

variable "subnet_1_name" {
  type        = string
  description = "The name of the subnet for the 1st availability zone."
}

variable "subnet_1_zone" {
  type        = string
  description = "The 1st availability zone."
}

################################################################################

variable "subnet_2_id" {
  type        = string
  description = "The ID of the subnet for the 2nd availability zone."
}

variable "subnet_2_name" {
  type        = string
  description = "The name of the subnet for the 2nd availability zone."
}

variable "subnet_2_zone" {
  type        = string
  description = "The 2nd availability zone."
}

################################################################################

variable "subnet_3_id" {
  type        = string
  description = "The ID of the subnet for the 3rd availability zone."
}

variable "subnet_3_name" {
  type        = string
  description = "The name of the subnet for the 3rd availability zone."
}

variable "subnet_3_zone" {
  type        = string
  description = "The 3rd availability zone."
}

################################################################################
# Optional Variables
################################################################################

variable "ami" {
  type        = string
  default     = "kope.io/k8s-1.14-debian-stretch-amd64-hvm-ebs-2019-09-26"
  description = "The Amazon AMI image to use for Kubernetes masters and nodes."
}

variable "private" {
  type        = bool
  default     = true
  description = "Whether to make a private or public topology Kubernetes cluster. If 'true', provided subnets must be private, otherwise provided subnets must be public."
}

variable "release" {
  type        = string
  default     = "1.14.8"
  description = "Kubernetes version to run on the cluster."
}

variable "ipv4_cidr" {
  type        = string
  default     = "192.168.0.0/16"
  description = "The internal IPv4 CIDR range Kubernetes should use for containers and services."
}

variable "timestamp" {
  type        = string
  default     = "2019-10-22T16:06:00Z"
  description = "The template creation date. Should be manually changed when the template configuration is intentionally changed. An automatically generated timestamp is not used to prevent terraform constantly re-rendering the template."
}

variable "authorization" {
  type        = string
  default     = "rbac"
  description = "The Kubernetes cluster default authorisation type."
}

################################################################################

variable "nodegroup_name" {
  type        = string
  default     = "nodes"
  description = "Name of the node group to be created."
}

################################################################################

variable "master_instance_type" {
  type        = string
  default     = "m5.large"
  description = "The Amazon EC2 instance type for the Kubernetes masters."
}

variable "nodegroup_instance_type" {
  type        = string
  default     = "m5.large"
  description = "The Amazon EC2 instance type for the Kubernetes nodes."
}

################################################################################

variable "master_security_groups" {
  type        = list(string)
  default     = []
  description = "A list of security groups to additionally attach to Kubernetes masters."
}

variable "nodegroup_security_groups" {
  type        = list(string)
  default     = []
  description = "A list of security groups to additionally attach to Kubernetes nodes."
}

################################################################################

variable "master_scaling_min_per_zone" {
  type        = number
  default     = 1
  description = "Set the initial amount of Kubernetes masters to provision in each availability zone. This also acts as the minimum scaling limit."
}

variable "master_scaling_max_per_zone" {
  type        = number
  default     = 1
  description = "Set the maximum scaling limit of Kubernetes masters per each availability zone."
}

variable "nodegroup_scaling_min_per_zone" {
  type        = number
  default     = 0
  description = "Set the initial amount of Kubernetes nodes to provision in each availability zone. This also acts as the minimum scaling limit."
}

variable "nodegroup_scaling_max_per_zone" {
  type        = number
  default     = 10
  description = "Set the maximum scaling limit of Kubernetes nodes per each availability zone."
}

################################################################################

variable "master_iam_policy" {
  type        = string
  default     = ""
  description = "The additional set of IAM policies to apply to the Kubernetes masters. Must be a string formatted as a JSON list."
}

variable "nodegroup_iam_policy" {
  type        = string
  default     = ""
  description = "The additional set of IAM policies to apply to the Kubernetes nodes. Must be a string formatted as a JSON list."
}

################################################################################

variable "default_public_ipv4_cidrs" {
  type        = list(string)
  default     = ["0.0.0.0/0"]
  description = "The default IPv4 CIDR ranges to use for 'api_ipv4_cidrs' and 'ssh_ipv4_cidrs' when 'private' is 'false'."
}

variable "default_private_ipv4_cidrs" {
  type        = list(string)
  default     = ["10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16"]
  description = "The default IPv4 CIDR ranges to use for 'api_ipv4_cidrs' and 'ssh_ipv4_cidrs' when 'private' is 'true'."
}

################################################################################

variable "api_ipv4_cidrs" {
  type        = list(string)
  default     = []
  description = "Override the IPv4 CIDR ranges that are allowed to contact the Kubernetes API load balancer."
}

variable "ssh_ipv4_cidrs" {
  type        = list(string)
  default     = []
  description = "Override the IPv4 CIDR ranges that are allowed to SSH into the Kubernetes masters and nodes."
}

################################################################################

variable "system_reserved_cpu" {
  type        = number
  default     = 100
  description = "The amount of CPU to reserve for the operating system, in millicores."
}

variable "system_reserved_memory_mb" {
  type        = number
  default     = 512
  description = "The amount of RAM to reserve for the operating system in megabytes."
}

variable "system_reserved_disk_gb" {
  type        = number
  default     = 1
  description = "The amount of disk space to reserve for the operating system in gigabytes."
}

################################################################################

variable "kube_reserved_cpu" {
  type        = number
  default     = 100
  description = "The amount of CPU to reserve for the Kubernetes core components, in millicores."
}

variable "kube_reserved_memory_mb" {
  type        = number
  default     = 256
  description = "The amount of RAM to reserve for the Kubernetes core components in megabytes."
}

variable "kube_reserved_disk_gb" {
  type        = number
  default     = 1
  description = "The amount of disk space to reserve for the Kubernetes core components in gigabytes."
}

################################################################################

variable "evict_hard_memory_mb" {
  type        = number
  default     = 256
  description = "When the amount of RAM (in megabytes) that is available on a Kubernetes node before hitting any limits becomes equal to or less than this value, running pods will begin to be evicted from the node."
}

variable "evict_hard_nodefs_space_percent" {
  type        = number
  default     = 5
  description = "When the amount of space (in percent) that is available on a Kubernetes node primary filesystem before hitting any limits becomes equal to or less than this value, running pods will begin to be evicted from the node."
}

variable "evict_hard_nodefs_inode_percent" {
  type        = number
  default     = 5
  description = "When the amount of inodes (in percent) that is available on a Kubernetes node primary filesystem before hitting any limits becomes equal to or less than this value, running pods will begin to be evicted from the node."
}

variable "evict_hard_imagefs_space_percent" {
  type        = number
  default     = 5
  description = "When the amount of space (in percent) that is available on a Kubernetes node image filesystem before hitting any limits becomes equal to or less than this value, running pods will begin to be evicted from the node."
}

variable "evict_hard_imagefs_inode_percent" {
  type        = number
  default     = 5
  description = "When the amount of inodes (in percent) that is available on a Kubernetes node image filesystem before hitting any limits becomes equal to or less than this value, running pods will begin to be evicted from the node."
}

################################################################################

variable "hpa_target_threshold" {
  type        = number
  default     = 0.2
  description = "What is the target resource threshold before scaling up or down. For example, if a CPU target was 50% and the 'hpa_target_threshold' is '0.2', scale down would occur when CPU is 40% and scale up when the CPU is 60%."
}

variable "hpa_sync_period_seconds" {
  type        = number
  default     = 30
  description = "How long should Kubernetes Horizontal Pod Autoscalers wait after scaling up before being able to scale up again."
}

################################################################################

variable "audit_policy_name" {
  type        = string
  default     = "none"
  description = "Name of the built-in Kubernetes audit policy to use. Currently only supports 'none'."
}

variable "audit_policy_suffix" {
  type        = string
  default     = ".yaml"
  description = "What suffix does the built-in Kubernetes audit policy file have."
}

################################################################################

variable "audit_log_file" {
  type        = string
  default     = "-"
  description = "Where on the Kubernetes masters should the audit log file be placed or '-' for stdout."
}

variable "audit_log_max_age" {
  type        = number
  default     = 14
  description = "What is the maximum age of audit events before they get rotated from the log file (they may get rotated sooner due to 'audit_log_max_size_mb'). Ignored if 'audit_log_file' is '-'."
}

variable "audit_log_max_backups" {
  type        = number
  default     = 1
  description = "The maximum number of backups to keep for the Kubernetes audit log. Ignored if 'audit_log_file' is '-'."
}

variable "audit_log_max_size_mb" {
  type        = number
  default     = 128
  description = "What is the maximum size of the Kubernetes audit log before it gets rotated (it may get rotated at a smaller size due to 'audit_log_max_age'). Ignored if 'audit_log_file' is '-'."
}

################################################################################

variable "template_name" {
  type        = string
  default     = "kops.tpl"
  description = "Name of the source template to render. The template must be present within this module. Currently only 'kops.tpl' is available."
}

variable "template_path" {
  type        = string
  default     = null
  description = "User specified source template to render. This must be a full path to a template file. This value takes precedence over 'template_name'."
}

################################################################################

variable "sums_table_hash" {
  type        = string
  default     = "ID"
  description = "Primary key of the DynamoDB table 'sums_table_name'."
}

variable "sums_table_value" {
  type        = string
  default     = "CONTENT"
  description = "Value key of the DynamoDB table 'sums_table_name'."
}

################################################################################

variable "export_key_suffix" {
  type        = string
  default     = ".pub"
  description = "When exporting the public SSH key to the S3 bucket 'data_bucket' what should be suffixed to 'vpc'."
}

variable "export_spec_suffix" {
  type        = string
  default     = ".yaml"
  description = "When exporting the YAML manifest to the S3 bucket 'data_bucket' what should be suffixed to 'vpc'."
}

################################################################################
# Locals
################################################################################

locals {
  yaml_master_security_groups    = join("\n", formatlist("  - %s", var.master_security_groups))
  yaml_nodegroup_security_groups = join("\n", formatlist("  - %s", var.nodegroup_security_groups))
  yaml_master_iam_policy         = var.master_iam_policy == "" ? "" : format("    master: |\n      %s", indent(6, var.master_iam_policy))
  yaml_nodegroup_iam_policy      = var.nodegroup_iam_policy == "" ? "" : format("    node: |\n      %s", indent(6, var.nodegroup_iam_policy))
  yaml_api_ipv4_cidrs            = var.private ? join("\n", formatlist("  - %s", coalescelist(var.api_ipv4_cidrs, var.default_private_ipv4_cidrs))) : join("\n", formatlist("  - %s", coalescelist(var.api_ipv4_cidrs, var.default_public_ipv4_cidrs)))
  yaml_ssh_ipv4_cidrs            = var.private ? join("\n", formatlist("  - %s", coalescelist(var.ssh_ipv4_cidrs, var.default_private_ipv4_cidrs))) : join("\n", formatlist("  - %s", coalescelist(var.ssh_ipv4_cidrs, var.default_public_ipv4_cidrs)))
  yaml_audit_policy              = indent(6, file(format("%s/auditpolicies/%s%s", path.module, var.audit_policy_name, var.audit_policy_suffix)))
  export_spec_id                 = format("%s%s", var.vpc, var.export_spec_suffix)
  export_key_id                  = format("%s%s", var.vpc, var.export_key_suffix)
  template_name                  = var.template_path == "" ? var.template_name : ""
  template_path                  = coalesce(var.template_path, format("%s/%s", path.module, var.template_name))
}

################################################################################
# Data Sources
################################################################################

data "template_file" "scope" {
  template = file(local.template_path)

  vars = {
    "name"                             = var.name
    "owner"                            = var.owner
    "company"                          = var.company
    "bucket"                           = var.state_bucket_name
    "ami"                              = var.ami
    "private"                          = var.private
    "release"                          = var.release
    "ipv4_cidr"                        = var.ipv4_cidr
    "timestamp"                        = var.timestamp
    "authorization"                    = var.authorization
    "vpc_id"                           = var.vpc_id
    "vpc_ipv4_cidr"                    = var.vpc_ipv4_cidr
    "subnet_1_id"                      = var.subnet_1_id
    "subnet_1_name"                    = var.subnet_1_name
    "subnet_1_zone"                    = var.subnet_1_zone
    "subnet_2_id"                      = var.subnet_2_id
    "subnet_2_name"                    = var.subnet_2_name
    "subnet_2_zone"                    = var.subnet_2_zone
    "subnet_3_id"                      = var.subnet_3_id
    "subnet_3_name"                    = var.subnet_3_name
    "subnet_3_zone"                    = var.subnet_3_zone
    "nodegroup_name"                   = var.nodegroup_name
    "master_instance_type"             = var.master_instance_type
    "nodegroup_instance_type"          = var.nodegroup_instance_type
    "master_security_groups"           = local.yaml_master_security_groups
    "nodegroup_security_groups"        = local.yaml_nodegroup_security_groups
    "master_scaling_min_per_zone"      = var.master_scaling_min_per_zone
    "master_scaling_max_per_zone"      = var.master_scaling_max_per_zone
    "nodegroup_scaling_min_per_zone"   = var.nodegroup_scaling_min_per_zone
    "nodegroup_scaling_max_per_zone"   = var.nodegroup_scaling_max_per_zone
    "master_iam_policy"                = local.yaml_master_iam_policy
    "nodegroup_iam_policy"             = local.yaml_nodegroup_iam_policy
    "api_ipv4_cidrs"                   = local.yaml_api_ipv4_cidrs
    "ssh_ipv4_cidrs"                   = local.yaml_ssh_ipv4_cidrs
    "system_reserved_cpu"              = var.system_reserved_cpu
    "system_reserved_memory_mb"        = var.system_reserved_memory_mb
    "system_reserved_disk_gb"          = var.system_reserved_disk_gb
    "kube_reserved_cpu"                = var.kube_reserved_cpu
    "kube_reserved_memory_mb"          = var.kube_reserved_memory_mb
    "kube_reserved_disk_gb"            = var.kube_reserved_disk_gb
    "evict_hard_memory_mb"             = var.evict_hard_memory_mb
    "evict_hard_nodefs_space_percent"  = var.evict_hard_nodefs_space_percent
    "evict_hard_nodefs_inode_percent"  = var.evict_hard_nodefs_inode_percent
    "evict_hard_imagefs_space_percent" = var.evict_hard_imagefs_space_percent
    "evict_hard_imagefs_inode_percent" = var.evict_hard_imagefs_inode_percent
    "hpa_target_threshold"             = var.hpa_target_threshold
    "hpa_sync_period_seconds"          = var.hpa_sync_period_seconds
    "audit_policy"                     = local.yaml_audit_policy
    "audit_log_file"                   = var.audit_log_file
    "audit_log_max_age"                = var.audit_log_max_age
    "audit_log_max_backups"            = var.audit_log_max_backups
    "audit_log_max_size_mb"            = var.audit_log_max_size_mb
  }
}

################################################################################
# Resources
################################################################################

resource "aws_s3_object" "export-spec" {
  key     = local.export_spec_id
  etag    = md5(data.template_file.scope.rendered)
  bucket  = var.data_bucket_name
  content = data.template_file.scope.rendered
}

################################################################################

resource "aws_s3_object" "export-key" {
  key     = local.export_key_id
  etag    = md5(var.key)
  bucket  = var.data_bucket_name
  content = var.key
}

################################################################################

resource "aws_dynamodb_table_item" "export-spec-sum" {
  table_name = var.sums_table_name
  hash_key   = var.sums_table_hash
  item       = <<DYNAMOITEM
{
  "${var.sums_table_hash}": {"S": "${md5(local.export_spec_id)}"},
  "${var.sums_table_value}": {"S": "${aws_s3_object.export-spec.etag}"}
}
DYNAMOITEM
}

################################################################################

resource "aws_dynamodb_table_item" "export-key-sum" {
  table_name = var.sums_table_name
  hash_key   = var.sums_table_hash
  item       = <<DYNAMOITEM
{
  "${var.sums_table_hash}": {"S": "${md5(local.export_key_id)}"},
  "${var.sums_table_value}": {"S": "${aws_s3_object.export-key.etag}"}
}
DYNAMOITEM
}

################################################################################
# Outputs
################################################################################

output "name" {
  value = var.name
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "sums_table_name" {
  value = var.sums_table_name
}

output "data_bucket_name" {
  value = var.data_bucket_name
}

output "state_bucket_name" {
  value = var.state_bucket_name
}

output "data_bucket_url" {
  value = format("s3://%s/", var.data_bucket_name)
}

output "state_bucket_url" {
  value = format("s3://%s/", var.state_bucket_name)
}

################################################################################

output "vpc" {
  value = var.vpc
}

output "vpc_id" {
  value = var.vpc_id
}

output "vpc_ipv4_cidr" {
  value = var.vpc_ipv4_cidr
}

################################################################################

output "subnet_1_id" {
  value = var.subnet_1_id
}

output "subnet_1_name" {
  value = var.subnet_1_name
}

output "subnet_1_zone" {
  value = var.subnet_1_zone
}

################################################################################

output "subnet_2_id" {
  value = var.subnet_2_id
}

output "subnet_2_name" {
  value = var.subnet_2_name
}

output "subnet_2_zone" {
  value = var.subnet_2_zone
}

################################################################################

output "subnet_3_id" {
  value = var.subnet_3_id
}

output "subnet_3_name" {
  value = var.subnet_3_name
}

output "subnet_3_zone" {
  value = var.subnet_3_zone
}

################################################################################

output "ami" {
  value = var.ami
}

output "key" {
  sensitive = true
  value     = var.key
}

output "private" {
  value = var.private
}

output "release" {
  value = var.release
}

output "ipv4_cidr" {
  value = var.ipv4_cidr
}

output "timestamp" {
  value = var.timestamp
}

output "authorization" {
  value = var.authorization
}

################################################################################

output "nodegroup_name" {
  value = var.nodegroup_name
}

################################################################################

output "master_instance_type" {
  value = var.master_instance_type
}

output "nodegroup_instance_type" {
  value = var.nodegroup_instance_type
}

################################################################################

output "master_security_groups" {
  value = var.master_security_groups
}

output "nodegroup_security_groups" {
  value = var.nodegroup_security_groups
}

################################################################################

output "master_scaling_min_per_zone" {
  value = var.master_scaling_min_per_zone
}

output "master_scaling_max_per_zone" {
  value = var.master_scaling_max_per_zone
}

output "nodegroup_scaling_min_per_zone" {
  value = var.nodegroup_scaling_min_per_zone
}

output "nodegroup_scaling_max_per_zone" {
  value = var.nodegroup_scaling_max_per_zone
}

################################################################################

output "master_iam_policy" {
  sensitive = true
  value     = var.master_iam_policy
}

output "nodegroup_iam_policy" {
  sensitive = true
  value     = var.nodegroup_iam_policy
}

################################################################################

output "api_ipv4_cidrs" {
  value = var.private ? coalescelist(var.api_ipv4_cidrs, var.default_private_ipv4_cidrs) : coalescelist(var.api_ipv4_cidrs, var.default_public_ipv4_cidrs)
}

output "ssh_ipv4_cidrs" {
  value = var.private ? coalescelist(var.ssh_ipv4_cidrs, var.default_private_ipv4_cidrs) : coalescelist(var.ssh_ipv4_cidrs, var.default_public_ipv4_cidrs)
}

################################################################################

output "system_reserved_cpu" {
  value = var.system_reserved_cpu
}

output "system_reserved_memory_mb" {
  value = var.system_reserved_memory_mb
}

output "system_reserved_disk_gb" {
  value = var.system_reserved_disk_gb
}

################################################################################

output "kube_reserved_cpu" {
  value = var.kube_reserved_cpu
}

output "kube_reserved_memory_mb" {
  value = var.kube_reserved_memory_mb
}

output "kube_reserved_disk_gb" {
  value = var.kube_reserved_disk_gb
}

################################################################################

output "evict_hard_memory_mb" {
  value = var.evict_hard_memory_mb
}

output "evict_hard_nodefs_space_percent" {
  value = var.evict_hard_nodefs_space_percent
}

output "evict_hard_nodefs_inode_percent" {
  value = var.evict_hard_nodefs_inode_percent
}

output "evict_hard_imagefs_space_percent" {
  value = var.evict_hard_imagefs_space_percent
}

output "evict_hard_imagefs_inode_percent" {
  value = var.evict_hard_imagefs_inode_percent
}

################################################################################

output "hpa_target_threshold" {
  value = var.hpa_target_threshold
}

output "hpa_sync_period_seconds" {
  value = var.hpa_sync_period_seconds
}

################################################################################

output "audit_policy_name" {
  value = var.audit_policy_name
}

output "audit_policy_suffix" {
  value = var.audit_policy_suffix
}

output "audit_policy_yaml" {
  sensitive = true
  value     = local.yaml_audit_policy
}

################################################################################

output "audit_log_file" {
  value = var.audit_log_file
}

output "audit_log_max_age" {
  value = var.audit_log_max_age
}

output "audit_log_max_backups" {
  value = var.audit_log_max_backups
}

output "audit_log_max_size_mb" {
  value = var.audit_log_max_size_mb
}

################################################################################

output "template_name" {
  value = local.template_name
}

output "template_path" {
  value = local.template_path
}

################################################################################

output "sums_table_hash" {
  value = var.sums_table_hash
}

output "sums_table_value" {
  value = var.sums_table_value
}

################################################################################

output "export_key_suffix" {
  value = var.export_key_suffix
}

output "export_spec_suffix" {
  value = var.export_spec_suffix
}

################################################################################

output "export_spec_id" {
  value = aws_s3_object.export-spec.id
}

output "export_spec_url" {
  value = format("s3://%s/%s", var.data_bucket_name, aws_s3_object.export-spec.id)
}

output "export_spec_etag" {
  value = aws_s3_object.export-spec.etag
}

################################################################################

output "export_key_id" {
  value = aws_s3_object.export-key.id
}

output "export_key_url" {
  value = format("s3://%s/%s", var.data_bucket_name, aws_s3_object.export-key.id)
}

output "export_key_etag" {
  value = aws_s3_object.export-key.etag
}

################################################################################
