###
### Terraform Managed
###
################################################################################
################################################################################

apiVersion: kops/v1alpha2
kind: Cluster
metadata:
  creationTimestamp: ${timestamp}
  name: ${name}
  cluster_name: ${name}
spec:
  additionalPolicies:
${master_iam_policy   }
${nodegroup_iam_policy}
  api:
    loadBalancer:
      type: ${private ? "Internal" : "Public"}
  authorization:
    ${authorization}: {}
  channel: stable
  cloudProvider: aws
  configBase: s3://${bucket}/${name}
  etcdClusters:
  - etcdMembers:
    - instanceGroup: masters-${subnet_zone}
      name: ${subnet_zone}
    name: main
  - etcdMembers:
    - instanceGroup: masters-${subnet_zone}
      name: ${subnet_zone}
    name: events
  fileAssets:
  - name: audit-policy-file
    path: /srv/kubernetes/audit-policy.yaml
    roles:
    - Master
    content: |
      ${audit_policy}
  iam:
    allowContainerRegistry: true
    legacy: false
  kubeAPIServer:
    auditPolicyFile: /srv/kubernetes/audit-policy.yaml
    auditLogPath: ${      audit_log_file       }
    auditLogMaxAge: ${    audit_log_max_age    }
    auditLogMaxBackups: ${audit_log_max_backups}
    auditLogMaxSize: ${   audit_log_max_size_mb}
  kubeControllerManager:
    horizontalPodAutoscalerTolerance: ${ hpa_target_threshold}
    horizontalPodAutoscalerSyncPeriod: ${hpa_sync_period     }
  kubelet:
    anonymousAuth: false
    authorizationMode: Webhook
    authenticationTokenWebhook: true
    evictionHard: memory.available<${evict_hard_memory_mb}Mi,nodefs.available<${evict_hard_nodefs_space_percent}%,nodefs.inodesFree<${evict_hard_nodefs_inode_percent}%,imagefs.available<${evict_hard_imagefs_space_percent}%,imagefs.inodesFree<${evict_hard_imagefs_inode_percent}%
    kubeReserved:
        cpu: "${              kube_reserved_cpu      }m"
        memory: "${           kube_reserved_memory_mb}Mi"
        ephemeral-storage: "${kube_reserved_disk_gb  }Gi"
    systemReserved:
        cpu: "${              system_reserved_cpu      }m"
        memory: "${           system_reserved_memory_mb}Mi"
        ephemeral-storage: "${system_reserved_disk_gb  }Gi"
    enforceNodeAllocatable: "pods"
  kubernetesApiAccess:
${api_ipv4_cidrs}
  kubernetesVersion: ${release}
  masterInternalName: api.internal.${name}
  masterPublicName: api.${name}
  networkCIDR: ${vpc_ipv4_cidr}
  networkID: ${vpc_id}
  networking:
    weave:
      mtu: 8912
  nonMasqueradeCIDR: ${cidr}
  sshAccess:
${ssh_ipv4_cidrs}
  subnets:
  - id  : ${subnet_id                     }
    name: ${subnet_name                   }
    type: ${private ? "Private" : "Public"}
    zone: ${subnet_zone                   }
  topology:
    dns:
      type: ${ private ? "Private" : "Public"}
    masters: ${private ? "private" : "public"}
    nodes: ${  private ? "private" : "public"}

################################################################################
################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: masters-${subnet_zone}
spec:
  additionalSecurityGroups:
${master_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
  image: ${      ami                 }
  machineType: ${master_instance_type}
  maxSize: ${    master_scaling_max  }
  minSize: ${    master_scaling_min  }
  nodeLabels:
    bitservices.io/instancegroup-id: masters-${subnet_zone}
    bitservices.io/instancegroup-name: masters
    bitservices.io/instancegroup-type: masters
  role: Master
  subnets:
  - ${subnet_name}

################################################################################
################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: ${nodegroup_name}-${subnet_zone}
spec:
  additionalSecurityGroups:
${nodegroup_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
    kubernetes.io/cluster/${name}: ${name}
    k8s.io/cluster-autoscaler/enabled: "true"
  image: ${      ami                    }
  machineType: ${nodegroup_instance_type}
  maxSize: ${    nodegroup_scaling_max  }
  minSize: ${    nodegroup_scaling_min  }
  nodeLabels:
    bitservices.io/instancegroup-name: ${nodegroup_name}
    bitservices.io/instancegroup-type: nodes
  role: Node
  subnets:
  - ${subnet_name}

################################################################################
################################################################################
