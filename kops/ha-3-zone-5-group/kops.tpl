###
### Terraform Managed
###
################################################################################
################################################################################

apiVersion: kops/v1alpha2
kind: Cluster
metadata:
  creationTimestamp: ${timestamp}
  name: ${name}
  cluster_name: ${name}
spec:
  additionalPolicies:
${master_iam_policy   }
${nodegroup_iam_policy}
  api:
    loadBalancer:
      type: ${private ? "Internal" : "Public"}
  authorization:
    ${authorization}: {}
  channel: stable
  cloudProvider: aws
  configBase: s3://${bucket}/${name}
  etcdClusters:
  - etcdMembers:
    - instanceGroup: masters-${subnet_1_zone}
      name: ${subnet_1_zone}
    - instanceGroup: masters-${subnet_2_zone}
      name: ${subnet_2_zone}
    - instanceGroup: masters-${subnet_3_zone}
      name: ${subnet_3_zone}
    name: main
  - etcdMembers:
    - instanceGroup: masters-${subnet_1_zone}
      name: ${subnet_1_zone}
    - instanceGroup: masters-${subnet_2_zone}
      name: ${subnet_2_zone}
    - instanceGroup: masters-${subnet_3_zone}
      name: ${subnet_3_zone}
    name: events
  fileAssets:
  - name: audit-policy-file
    path: /srv/kubernetes/audit-policy.yaml
    roles:
    - Master
    content: |
      ${audit_policy}
  iam:
    allowContainerRegistry: true
    legacy: false
  kubeAPIServer:
    auditPolicyFile: /srv/kubernetes/audit-policy.yaml
    auditLogPath: ${      audit_log_file       }
    auditLogMaxAge: ${    audit_log_max_age    }
    auditLogMaxBackups: ${audit_log_max_backups}
    auditLogMaxSize: ${   audit_log_max_size_mb}
  kubeControllerManager:
    horizontalPodAutoscalerTolerance: ${ hpa_target_threshold}
    horizontalPodAutoscalerSyncPeriod: ${hpa_sync_period     }
  kubelet:
    anonymousAuth: false
    authorizationMode: Webhook
    authenticationTokenWebhook: true
    evictionHard: memory.available<${evict_hard_memory_mb}Mi,nodefs.available<${evict_hard_nodefs_space_percent}%,nodefs.inodesFree<${evict_hard_nodefs_inode_percent}%,imagefs.available<${evict_hard_imagefs_space_percent}%,imagefs.inodesFree<${evict_hard_imagefs_inode_percent}%
    kubeReserved:
        cpu: "${              kube_reserved_cpu      }m"
        memory: "${           kube_reserved_memory_mb}Mi"
        ephemeral-storage: "${kube_reserved_disk_gb  }Gi"
    systemReserved:
        cpu: "${              system_reserved_cpu      }m"
        memory: "${           system_reserved_memory_mb}Mi"
        ephemeral-storage: "${system_reserved_disk_gb  }Gi"
    enforceNodeAllocatable: "pods"
  kubernetesApiAccess:
${api_ipv4_cidrs}
  kubernetesVersion: ${release}
  masterInternalName: api.internal.${name}
  masterPublicName: api.${name}
  networkCIDR: ${vpc_ipv4_cidr}
  networkID: ${vpc_id}
  networking:
    weave:
      mtu: 8912
  nonMasqueradeCIDR: ${cidr}
  sshAccess:
${ssh_ipv4_cidrs}
  subnets:
  - id  : ${subnet_1_id                   }
    name: ${subnet_1_name                 }
    type: ${private ? "Private" : "Public"}
    zone: ${subnet_1_zone                 }
  - id  : ${subnet_2_id                   }
    name: ${subnet_2_name                 }
    type: ${private ? "Private" : "Public"}
    zone: ${subnet_2_zone                 }
  - id  : ${subnet_3_id                   }
    name: ${subnet_3_name                 }
    type: ${private ? "Private" : "Public"}
    zone: ${subnet_3_zone                 }
  topology:
    dns:
      type: ${ private ? "Private" : "Public"}
    masters: ${private ? "private" : "public"}
    nodes: ${  private ? "private" : "public"}

################################################################################
################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: masters-${subnet_1_zone}
spec:
  additionalSecurityGroups:
${master_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
  image: ${      ami                        }
  machineType: ${master_instance_type       }
  maxSize: ${    master_scaling_max_per_zone}
  minSize: ${    master_scaling_min_per_zone}
  nodeLabels:
    bitservices.io/instancegroup-id: masters-${subnet_1_zone}
    bitservices.io/instancegroup-name: masters
    bitservices.io/instancegroup-type: masters
  role: Master
  subnets:
  - ${subnet_1_name}

################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: masters-${subnet_2_zone}
spec:
  additionalSecurityGroups:
${master_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
  image: ${      ami                        }
  machineType: ${master_instance_type       }
  maxSize: ${    master_scaling_max_per_zone}
  minSize: ${    master_scaling_min_per_zone}
  nodeLabels:
    bitservices.io/instancegroup-id: masters-${subnet_2_zone}
    bitservices.io/instancegroup-name: masters
    bitservices.io/instancegroup-type: masters
  role: Master
  subnets:
  - ${subnet_2_name}

################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: masters-${subnet_3_zone}
spec:
  additionalSecurityGroups:
${master_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
  image: ${      ami                        }
  machineType: ${master_instance_type       }
  maxSize: ${    master_scaling_max_per_zone}
  minSize: ${    master_scaling_min_per_zone}
  nodeLabels:
    bitservices.io/instancegroup-id: masters-${subnet_3_zone}
    bitservices.io/instancegroup-name: masters
    bitservices.io/instancegroup-type: masters
  role: Master
  subnets:
  - ${subnet_3_name}

################################################################################
################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: ${nodegroup_1_name}-${subnet_1_zone}
spec:
  additionalSecurityGroups:
${nodegroup_1_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
    kubernetes.io/cluster/${name}: ${name}
    k8s.io/cluster-autoscaler/enabled: "true"
  image: ${      ami                             }
  machineType: ${nodegroup_1_instance_type       }
  maxSize: ${    nodegroup_1_scaling_max_per_zone}
  minSize: ${    nodegroup_1_scaling_min_per_zone}
  nodeLabels:
    bitservices.io/instancegroup-name: ${nodegroup_1_name}
    bitservices.io/instancegroup-type: nodes
  role: Node
  subnets:
  - ${subnet_1_name}

################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: ${nodegroup_1_name}-${subnet_2_zone}
spec:
  additionalSecurityGroups:
${nodegroup_1_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
    kubernetes.io/cluster/${name}: ${name}
    k8s.io/cluster-autoscaler/enabled: "true"
  image: ${      ami                             }
  machineType: ${nodegroup_1_instance_type       }
  maxSize: ${    nodegroup_1_scaling_max_per_zone}
  minSize: ${    nodegroup_1_scaling_min_per_zone}
  nodeLabels:
    bitservices.io/instancegroup-name: ${nodegroup_1_name}
    bitservices.io/instancegroup-type: nodes
  role: Node
  subnets:
  - ${subnet_2_name}

################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: ${nodegroup_1_name}-${subnet_3_zone}
spec:
  additionalSecurityGroups:
${nodegroup_1_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
    kubernetes.io/cluster/${name}: ${name}
    k8s.io/cluster-autoscaler/enabled: "true"
  image: ${      ami                             }
  machineType: ${nodegroup_1_instance_type       }
  maxSize: ${    nodegroup_1_scaling_max_per_zone}
  minSize: ${    nodegroup_1_scaling_min_per_zone}
  nodeLabels:
    bitservices.io/instancegroup-name: ${nodegroup_1_name}
    bitservices.io/instancegroup-type: nodes
  role: Node
  subnets:
  - ${subnet_3_name}

################################################################################
################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: ${nodegroup_2_name}-${subnet_1_zone}
spec:
  additionalSecurityGroups:
${nodegroup_2_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
    kubernetes.io/cluster/${name}: ${name}
    k8s.io/cluster-autoscaler/enabled: "true"
  image: ${      ami                             }
  machineType: ${nodegroup_2_instance_type       }
  maxSize: ${    nodegroup_2_scaling_max_per_zone}
  minSize: ${    nodegroup_2_scaling_min_per_zone}
  nodeLabels:
    bitservices.io/instancegroup-name: ${nodegroup_2_name}
    bitservices.io/instancegroup-type: nodes
  role: Node
  subnets:
  - ${subnet_1_name}

################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: ${nodegroup_2_name}-${subnet_2_zone}
spec:
  additionalSecurityGroups:
${nodegroup_2_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
    kubernetes.io/cluster/${name}: ${name}
    k8s.io/cluster-autoscaler/enabled: "true"
  image: ${      ami                             }
  machineType: ${nodegroup_2_instance_type       }
  maxSize: ${    nodegroup_2_scaling_max_per_zone}
  minSize: ${    nodegroup_2_scaling_min_per_zone}
  nodeLabels:
    bitservices.io/instancegroup-name: ${nodegroup_2_name}
    bitservices.io/instancegroup-type: nodes
  role: Node
  subnets:
  - ${subnet_2_name}

################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: ${nodegroup_2_name}-${subnet_3_zone}
spec:
  additionalSecurityGroups:
${nodegroup_2_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
    kubernetes.io/cluster/${name}: ${name}
    k8s.io/cluster-autoscaler/enabled: "true"
  image: ${      ami                             }
  machineType: ${nodegroup_2_instance_type       }
  maxSize: ${    nodegroup_2_scaling_max_per_zone}
  minSize: ${    nodegroup_2_scaling_min_per_zone}
  nodeLabels:
    bitservices.io/instancegroup-name: ${nodegroup_2_name}
    bitservices.io/instancegroup-type: nodes
  role: Node
  subnets:
  - ${subnet_3_name}

################################################################################
################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: ${nodegroup_3_name}-${subnet_1_zone}
spec:
  additionalSecurityGroups:
${nodegroup_3_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
    kubernetes.io/cluster/${name}: ${name}
    k8s.io/cluster-autoscaler/enabled: "true"
  image: ${      ami                             }
  machineType: ${nodegroup_3_instance_type       }
  maxSize: ${    nodegroup_3_scaling_max_per_zone}
  minSize: ${    nodegroup_3_scaling_min_per_zone}
  nodeLabels:
    bitservices.io/instancegroup-name: ${nodegroup_3_name}
    bitservices.io/instancegroup-type: nodes
  role: Node
  subnets:
  - ${subnet_1_name}

################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: ${nodegroup_3_name}-${subnet_2_zone}
spec:
  additionalSecurityGroups:
${nodegroup_3_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
    kubernetes.io/cluster/${name}: ${name}
    k8s.io/cluster-autoscaler/enabled: "true"
  image: ${      ami                             }
  machineType: ${nodegroup_3_instance_type       }
  maxSize: ${    nodegroup_3_scaling_max_per_zone}
  minSize: ${    nodegroup_3_scaling_min_per_zone}
  nodeLabels:
    bitservices.io/instancegroup-name: ${nodegroup_3_name}
    bitservices.io/instancegroup-type: nodes
  role: Node
  subnets:
  - ${subnet_2_name}

################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: ${nodegroup_3_name}-${subnet_3_zone}
spec:
  additionalSecurityGroups:
${nodegroup_3_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
    kubernetes.io/cluster/${name}: ${name}
    k8s.io/cluster-autoscaler/enabled: "true"
  image: ${      ami                             }
  machineType: ${nodegroup_3_instance_type       }
  maxSize: ${    nodegroup_3_scaling_max_per_zone}
  minSize: ${    nodegroup_3_scaling_min_per_zone}
  nodeLabels:
    bitservices.io/instancegroup-name: ${nodegroup_3_name}
    bitservices.io/instancegroup-type: nodes
  role: Node
  subnets:
  - ${subnet_3_name}

################################################################################
################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: ${nodegroup_4_name}-${subnet_1_zone}
spec:
  additionalSecurityGroups:
${nodegroup_4_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
    kubernetes.io/cluster/${name}: ${name}
    k8s.io/cluster-autoscaler/enabled: "true"
  image: ${      ami                             }
  machineType: ${nodegroup_4_instance_type       }
  maxSize: ${    nodegroup_4_scaling_max_per_zone}
  minSize: ${    nodegroup_4_scaling_min_per_zone}
  nodeLabels:
    bitservices.io/instancegroup-name: ${nodegroup_4_name}
    bitservices.io/instancegroup-type: nodes
  role: Node
  subnets:
  - ${subnet_1_name}

################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: ${nodegroup_4_name}-${subnet_2_zone}
spec:
  additionalSecurityGroups:
${nodegroup_4_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
    kubernetes.io/cluster/${name}: ${name}
    k8s.io/cluster-autoscaler/enabled: "true"
  image: ${      ami                             }
  machineType: ${nodegroup_4_instance_type       }
  maxSize: ${    nodegroup_4_scaling_max_per_zone}
  minSize: ${    nodegroup_4_scaling_min_per_zone}
  nodeLabels:
    bitservices.io/instancegroup-name: ${nodegroup_4_name}
    bitservices.io/instancegroup-type: nodes
  role: Node
  subnets:
  - ${subnet_2_name}

################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: ${nodegroup_4_name}-${subnet_3_zone}
spec:
  additionalSecurityGroups:
${nodegroup_4_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
    kubernetes.io/cluster/${name}: ${name}
    k8s.io/cluster-autoscaler/enabled: "true"
  image: ${      ami                             }
  machineType: ${nodegroup_4_instance_type       }
  maxSize: ${    nodegroup_4_scaling_max_per_zone}
  minSize: ${    nodegroup_4_scaling_min_per_zone}
  nodeLabels:
    bitservices.io/instancegroup-name: ${nodegroup_4_name}
    bitservices.io/instancegroup-type: nodes
  role: Node
  subnets:
  - ${subnet_3_name}

################################################################################
################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: ${nodegroup_5_name}-${subnet_1_zone}
spec:
  additionalSecurityGroups:
${nodegroup_5_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
    kubernetes.io/cluster/${name}: ${name}
    k8s.io/cluster-autoscaler/enabled: "true"
  image: ${      ami                             }
  machineType: ${nodegroup_5_instance_type       }
  maxSize: ${    nodegroup_5_scaling_max_per_zone}
  minSize: ${    nodegroup_5_scaling_min_per_zone}
  nodeLabels:
    bitservices.io/instancegroup-name: ${nodegroup_5_name}
    bitservices.io/instancegroup-type: nodes
  role: Node
  subnets:
  - ${subnet_1_name}

################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: ${nodegroup_5_name}-${subnet_2_zone}
spec:
  additionalSecurityGroups:
${nodegroup_5_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
    kubernetes.io/cluster/${name}: ${name}
    k8s.io/cluster-autoscaler/enabled: "true"
  image: ${      ami                             }
  machineType: ${nodegroup_5_instance_type       }
  maxSize: ${    nodegroup_5_scaling_max_per_zone}
  minSize: ${    nodegroup_5_scaling_min_per_zone}
  nodeLabels:
    bitservices.io/instancegroup-name: ${nodegroup_5_name}
    bitservices.io/instancegroup-type: nodes
  role: Node
  subnets:
  - ${subnet_2_name}

################################################################################

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
metadata:
  creationTimestamp: ${timestamp}
  labels:
    kops.k8s.io/cluster: ${name}
  name: ${nodegroup_5_name}-${subnet_3_zone}
spec:
  additionalSecurityGroups:
${nodegroup_5_security_groups}
  cloudLabels:
    Owner: ${  owner  }
    Company: ${company}
    kubernetes.io/cluster/${name}: ${name}
    k8s.io/cluster-autoscaler/enabled: "true"
  image: ${      ami                             }
  machineType: ${nodegroup_5_instance_type       }
  maxSize: ${    nodegroup_5_scaling_max_per_zone}
  minSize: ${    nodegroup_5_scaling_min_per_zone}
  nodeLabels:
    bitservices.io/instancegroup-name: ${nodegroup_5_name}
    bitservices.io/instancegroup-type: nodes
  role: Node
  subnets:
  - ${subnet_3_name}

################################################################################
################################################################################
