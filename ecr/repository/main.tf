################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "The full name of the ECR repository."
}

################################################################################
# Optional Variables
################################################################################

variable "expire_untagged_enabled" {
  type        = bool
  default     = true
  description = "Enable or disable the lifecycle rule that will expire untagged Docker images."
}

variable "expire_untagged_type" {
  type        = string
  default     = "sinceImagePushed"
  description = "The rule type for the lifecycle rule that will expire untagged Docker images."
}

variable "expire_untagged_unit" {
  type        = string
  default     = "days"
  description = "The unit type that 'expire_untagged_value' represents. Only used if 'expire_untagged_type' is 'sinceImagePushed'."
}

variable "expire_untagged_value" {
  type        = number
  default     = 31
  description = "If 'expire_untagged_type' is 'imageCountMoreThan', this represents the number of untagged images before old ones are removed. if 'expire_untagged_type' is 'sinceImagePushed' this is the number of 'expire_untagged_unit' that must pass since the untagged images were pushed before they are removed."
}

variable "expire_untagged_priority" {
  type        = number
  default     = 1
  description = "The priority of the lifecycle rule that will expire untagged Docker images."
}

variable "expire_untagged_description" {
  type        = string
  default     = "Expire Untagged Images"
  description = "Override the default description of the lifecycle rule that will expire untagged Docker images."
}

################################################################################
# Locals
################################################################################

locals {
  policy                             = var.expire_untagged_type == "sinceImagePushed" ? local.expire_untagged_since_pushed_policy : local.expire_untagged_image_count_policy
  expire_untagged_image_count_policy = <<POLICY
{
    "rules": [
        {
            "rulePriority": ${var.expire_untagged_priority},
            "description": "${var.expire_untagged_description}",
            "selection": {
                "tagStatus": "untagged",
                "countType": "${var.expire_untagged_type}",
                "countNumber": ${var.expire_untagged_value}
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
POLICY

  expire_untagged_since_pushed_policy = <<POLICY
{
    "rules": [
        {
            "rulePriority": ${var.expire_untagged_priority},
            "description": "${var.expire_untagged_description}",
            "selection": {
                "tagStatus": "untagged",
                "countType": "${var.expire_untagged_type}",
                "countUnit": "${var.expire_untagged_unit}",
                "countNumber": ${var.expire_untagged_value}
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
POLICY
}

################################################################################
# Resources
################################################################################

resource "aws_ecr_repository" "scope" {
  name = var.name
}

################################################################################

resource "aws_ecr_lifecycle_policy" "expire_untagged" {
  count      = var.expire_untagged_enabled ? 1 : 0
  repository = aws_ecr_repository.scope.name
  policy     = local.policy
}

################################################################################
# Outputs
################################################################################

output "expire_untagged_enabled" {
  value = var.expire_untagged_enabled
}

output "expire_untagged_type" {
  value = var.expire_untagged_type
}

output "expire_untagged_unit" {
  value = var.expire_untagged_unit
}

output "expire_untagged_value" {
  value = var.expire_untagged_value
}

output "expire_untagged_priority" {
  value = var.expire_untagged_priority
}

output "expire_untagged_description" {
  value = var.expire_untagged_description
}

################################################################################

output "policy" {
  value = local.policy
}

################################################################################

output "arn" {
  value = aws_ecr_repository.scope.arn
}

output "url" {
  value = aws_ecr_repository.scope.repository_url
}

output "name" {
  value = aws_ecr_repository.scope.name
}

output "registry" {
  value = aws_ecr_repository.scope.registry_id
}

################################################################################
