<!----------------------------------------------------------------------------->

# ecr/repository

#### Manage [ECR] repositories

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/container/aws//ecr/repository`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_ecr_repository" {
  source = "gitlab.com/bitservices/container/aws//ecr/repository"
  name   = "foobar"
}
```

<!----------------------------------------------------------------------------->

[ECR]: https://aws.amazon.com/ecr/

<!----------------------------------------------------------------------------->
