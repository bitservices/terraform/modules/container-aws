################################################################################
# Required Variables
################################################################################

variable "policy" {
  type        = string
  description = "The policy JSON to apply to the ECR repository."
}

variable "repository" {
  type        = string
  description = "The full name of the ECR repository to apply this policy too."
}

################################################################################
# Resources
################################################################################

resource "aws_ecr_repository_policy" "scope" {
  policy     = var.policy
  repository = var.repository
}

################################################################################
# Outputs
################################################################################

output "policy" {
  value = var.policy
}

################################################################################

output "registry" {
  value = aws_ecr_repository_policy.scope.registry_id
}

output "repository" {
  value = aws_ecr_repository_policy.scope.repository
}

################################################################################
