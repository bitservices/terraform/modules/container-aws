<!----------------------------------------------------------------------------->

# ecr/repository-policy

#### Manage [ECR] repository policies

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/container/aws//ecr/repository-policy`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_ecr_repository" {
  source = "gitlab.com/bitservices/container/aws//ecr/repository"
  name   = "foobar"
}

module "my_ecr_repository_policy" {
  source     = "gitlab.com/bitservices/container/aws//ecr/repository-policy"
  repository = module.my_ecr_repository.name
  policy     = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "AllowCrossAccountPull",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::012345678912:root"
      },
      "Action": [
        "ecr:BatchCheckLayerAvailability",
        "ecr:BatchGetImage",
        "ecr:DescribeRepositories",
        "ecr:GetAuthorizationToken",
        "ecr:GetDownloadUrlForLayer",
        "ecr:GetRepositoryPolicy",
        "ecr:ListImages"
      ]
    }
  ]
}
POLICY
}
```

<!----------------------------------------------------------------------------->

[ECR]: https://aws.amazon.com/ecr/

<!----------------------------------------------------------------------------->
