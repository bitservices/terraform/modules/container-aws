<!----------------------------------------------------------------------------->

# container (aws)

<!----------------------------------------------------------------------------->

## Description

Provisions containerisation services on [AWS] such as [ECR], [ECS] and [EKS].

<!----------------------------------------------------------------------------->

## Modules

* [ecr/repository](ecr/repository/README.md) - Manage [ECR] repositories.
* [ecr/repository-policy](ecr/repository-policy/README.md) - Manage [ECR] repository policies.
* [eks/access](eks/access/README.md) - Manage Elastic [Kubernetes] Service ([EKS]) access entries.
* [eks/addon/aws-ebs-csi-driver](eks/addon/aws-ebs-csi-driver/README.md) - Manage the [AWS EBS CSI Driver] Elastic [Kubernetes] Service ([EKS]) addon.
* [eks/addon/aws-efs-csi-driver](eks/addon/aws-efs-csi-driver/README.md) - Manage the [AWS EFS CSI Driver] Elastic [Kubernetes] Service ([EKS]) addon.
* [eks/addon/coredns](eks/addon/coredns/README.md) - Manage the [CoreDNS] Elastic [Kubernetes] Service ([EKS]) addon.
* [eks/addon/kube-proxy](eks/addon/kube-proxy/README.md) - Manage the [Kube Proxy] Elastic [Kubernetes] Service ([EKS]) addon.
* [eks/addon/vpc-cni](eks/addon/vpc-cni/README.md) - Manage the [VPC CNI] Elastic [Kubernetes] Service ([EKS]) addon.
* [eks/cluster](eks/cluster/README.md) - Manage Elastic [Kubernetes] Service ([EKS]) clusters.
* [eks/node-group](eks/node-group/README.md) - Manage Elastic [Kubernetes] Service ([EKS]) node groups.
* [kops/ha-3-zone](kops/ha-3-zone/README.md) - Produces a [KOPS] template for a [Kubernetes] cluster based on a highly available 3 availability zone architecture that will run on [AWS].
* [kops/ha-3-zone-5-group](kops/ha-3-zone-5-group/README.md) - Produces a [KOPS] template for a [Kubernetes] cluster based on a highly available 3 availability zone architecture with 5 instance groups that will run on [AWS].
* [kops/std-1-zone](kops/std-1-zone/README.md) - Produces a [KOPS] template for a [Kubernetes] cluster based on a standard 1 availability zone architecture that will run on [AWS].


<!----------------------------------------------------------------------------->

[AWS]:                https://aws.amazon.com/
[ECR]:                https://aws.amazon.com/ecr/
[ECS]:                https://aws.amazon.com/ecs/
[EKS]:                https://aws.amazon.com/eks/
[KOPS]:               https://kops.sigs.k8s.io/
[CoreDNS]:            https://docs.aws.amazon.com/eks/latest/userguide/managing-coredns.html
[VPC CNI]:            https://docs.aws.amazon.com/eks/latest/userguide/managing-vpc-cni.html
[Kube Proxy]:         https://docs.aws.amazon.com/eks/latest/userguide/managing-kube-proxy.html
[Kubernetes]:         https://kubernetes.io/
[AWS EBS CSI Driver]: https://docs.aws.amazon.com/eks/latest/userguide/ebs-csi.html
[AWS EFS CSI Driver]: https://docs.aws.amazon.com/eks/latest/userguide/efs-csi.html

<!----------------------------------------------------------------------------->
