################################################################################
# Data Sources
################################################################################

data "aws_caller_identity" "scope" {}

################################################################################

data "aws_iam_session_context" "scope" {
  arn = data.aws_caller_identity.scope.arn
}

################################################################################
# Outputs
################################################################################

output "caller_identity_id" {
  value = data.aws_caller_identity.scope.user_id
}

output "caller_identity_arn" {
  value = data.aws_caller_identity.scope.arn
}

output "caller_identity_account" {
  value = data.aws_caller_identity.scope.account_id
}

################################################################################

output "caller_identity_role_id" {
  value = data.aws_iam_session_context.scope.issuer_id
}

output "caller_identity_role_arn" {
  value = data.aws_iam_session_context.scope.issuer_arn
}

output "caller_identity_role_name" {
  value = data.aws_iam_session_context.scope.issuer_name
}

output "caller_identity_role_session" {
  value = data.aws_iam_session_context.scope.session_name
}

################################################################################
