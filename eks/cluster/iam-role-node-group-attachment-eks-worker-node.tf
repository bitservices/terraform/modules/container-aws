################################################################################
# Optional Variables
################################################################################

variable "iam_role_node_group_attachment_eks_worker_node_policy_name" {
  type        = string
  default     = "AmazonEKSWorkerNodeMinimalPolicy"
  description = "The name of the built-in EKS worker node IAM policy to attach to the EKS node groups IAM role."
}

################################################################################
# Locals
################################################################################

locals {
  iam_role_node_group_attachment_eks_worker_node_policy_arn = format("arn:aws:iam::aws:policy/%s", var.iam_role_node_group_attachment_eks_worker_node_policy_name)
}

################################################################################
# Resources
################################################################################

resource "aws_iam_role_policy_attachment" "node_group_eks_worker_node" {
  count      = local.node_group_enabled ? 1 : 0
  role       = aws_iam_role.node_group[0].name
  policy_arn = local.iam_role_node_group_attachment_eks_worker_node_policy_arn
}

################################################################################
# Outputs
################################################################################

output "iam_role_node_group_attachment_eks_worker_node_policy_name" {
  value = var.iam_role_node_group_attachment_eks_worker_node_policy_name
}

################################################################################

output "iam_role_node_group_attachment_eks_worker_node_role" {
  value = length(aws_iam_role_policy_attachment.node_group_eks_worker_node) == 1 ? aws_iam_role_policy_attachment.node_group_eks_worker_node[0].role : null
}

output "iam_role_node_group_attachment_eks_worker_node_policy_arn" {
  value = length(aws_iam_role_policy_attachment.node_group_eks_worker_node) == 1 ? aws_iam_role_policy_attachment.node_group_eks_worker_node[0].policy_arn : null
}

################################################################################
