################################################################################
# Optional Variables
################################################################################

variable "iam_role_cluster_attachment_eks_vpc_resource_controller_policy_name" {
  type        = string
  default     = "AmazonEKSVPCResourceController"
  description = "The name of the built-in EKS VPC resource controller IAM policy to attach to the EKS cluster IAM role."
}

################################################################################
# Locals
################################################################################

locals {
  iam_role_cluster_attachment_eks_vpc_resource_controller_policy_arn = format("arn:aws:iam::aws:policy/%s", var.iam_role_cluster_attachment_eks_vpc_resource_controller_policy_name)
}

################################################################################
# Resources
################################################################################

resource "aws_iam_role_policy_attachment" "cluster_eks_vpc_resource_controller" {
  count = var.pod_security_groups ? 1 : 0

  role       = aws_iam_role.cluster.name
  policy_arn = local.iam_role_cluster_attachment_eks_vpc_resource_controller_policy_arn
}

################################################################################
# Outputs
################################################################################

output "iam_role_cluster_attachment_eks_vpc_resource_controller_role" {
  value = length(aws_iam_role_policy_attachment.cluster_eks_vpc_resource_controller) == 1 ? aws_iam_role_policy_attachment.cluster_eks_vpc_resource_controller[0].role : null
}

output "iam_role_cluster_attachment_eks_vpc_resource_controller_policy_arn" {
  value = length(aws_iam_role_policy_attachment.cluster_eks_vpc_resource_controller) == 1 ? aws_iam_role_policy_attachment.cluster_eks_vpc_resource_controller[0].policy_arn : null
}

################################################################################
