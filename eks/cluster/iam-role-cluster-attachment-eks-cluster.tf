################################################################################
# Optional Variables
################################################################################

variable "iam_role_cluster_attachment_eks_cluster_policy_name" {
  type        = string
  default     = "AmazonEKSClusterPolicy"
  description = "The name of the built-in EKS cluster IAM policy to attach to the EKS cluster IAM role."
}

################################################################################
# Locals
################################################################################

locals {
  iam_role_cluster_attachment_eks_cluster_policy_arn = format("arn:aws:iam::aws:policy/%s", var.iam_role_cluster_attachment_eks_cluster_policy_name)
}

################################################################################
# Resources
################################################################################

resource "aws_iam_role_policy_attachment" "cluster_eks_cluster" {
  role       = aws_iam_role.cluster.name
  policy_arn = local.iam_role_cluster_attachment_eks_cluster_policy_arn
}

################################################################################
# Outputs
################################################################################

output "iam_role_cluster_attachment_eks_cluster_role" {
  value = aws_iam_role_policy_attachment.cluster_eks_cluster.role
}

output "iam_role_cluster_attachment_eks_cluster_policy_arn" {
  value = aws_iam_role_policy_attachment.cluster_eks_cluster.policy_arn
}

################################################################################
