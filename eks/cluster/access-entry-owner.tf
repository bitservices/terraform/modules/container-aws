################################################################################
# Optional Variables
################################################################################

variable "access_entry_owner_type" {
  type        = string
  default     = "STANDARD"
  description = "The access entry type for the cluster owners administrator access. Must be either: 'STANDARD', 'EC2_LINUX', 'EC2_WINDOWS' or 'FARGATE_LINUX'."

  validation {
    condition     = contains(["STANDARD", "EC2_LINUX", "EC2_WINDOWS", "FARGATE_LINUX"], var.access_entry_owner_type)
    error_message = "The access entry type for the cluster owners administrator access must be either: 'STANDARD', 'EC2_LINUX', 'EC2_WINDOWS' or 'FARGATE_LINUX'."
  }
}

################################################################################
# Resources
################################################################################

resource "aws_eks_access_entry" "owner" {
  count         = var.authentication_owner ? 1 : 0
  type          = var.access_entry_owner_type
  cluster_name  = aws_eks_cluster.scope.name
  principal_arn = data.aws_iam_session_context.scope.issuer_arn

  tags = {
    "VPC"     = var.vpc
    "Owner"   = var.owner
    "Region"  = data.aws_region.scope.name
    "Cluster" = local.name
    "Company" = var.company
  }
}

################################################################################
# Outputs
################################################################################

output "access_entry_owner_id" {
  value = length(aws_eks_access_entry.owner) == 1 ? aws_eks_access_entry.owner[0].id : null
}

output "access_entry_owner_arn" {
  value = length(aws_eks_access_entry.owner) == 1 ? aws_eks_access_entry.owner[0].access_entry_arn : null
}

output "access_entry_owner_tags" {
  value = length(aws_eks_access_entry.owner) == 1 ? aws_eks_access_entry.owner[0].tags_all : null
}

output "access_entry_owner_type" {
  value = length(aws_eks_access_entry.owner) == 1 ? aws_eks_access_entry.owner[0].type : null
}

output "access_entry_owner_groups" {
  value = length(aws_eks_access_entry.owner) == 1 ? aws_eks_access_entry.owner[0].kubernetes_groups : null
}

output "access_entry_owner_cluster" {
  value = length(aws_eks_access_entry.owner) == 1 ? aws_eks_access_entry.owner[0].cluster_name : null
}

output "access_entry_owner_created" {
  value = length(aws_eks_access_entry.owner) == 1 ? aws_eks_access_entry.owner[0].created_at : null
}

output "access_entry_owner_modified" {
  value = length(aws_eks_access_entry.owner) == 1 ? aws_eks_access_entry.owner[0].modified_at : null
}

output "access_entry_owner_username" {
  value = length(aws_eks_access_entry.owner) == 1 ? aws_eks_access_entry.owner[0].user_name : null
}

output "access_entry_owner_principal" {
  value = length(aws_eks_access_entry.owner) == 1 ? aws_eks_access_entry.owner[0].principal_arn : null
}

################################################################################
