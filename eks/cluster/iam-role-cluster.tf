################################################################################
# Optional Variables
################################################################################

variable "iam_role_cluster_name" {
  type        = string
  default     = null
  description = "The full name of the EKS clusters IAM role."
}

variable "iam_role_cluster_path" {
  type        = string
  default     = "/"
  description = "The IAM path to the EKS clusters role."
}

variable "iam_role_cluster_class" {
  type        = string
  default     = "cluster"
  description = "The identifier for the EKS clusters IAM role. This is appended to the EKS clusters name. Ignored if 'iam_role_cluster_name' is set."
}

variable "iam_role_cluster_description" {
  type        = string
  default     = "Managed by Terraform"
  description = "The description of the EKS clusters IAM role."
}

variable "iam_role_cluster_force_detach" {
  type        = bool
  default     = false
  description = "Specifies to force detaching any policies the EKS clusters IAM role has before destroying it."
}

################################################################################
# Locals
################################################################################

locals {
  iam_role_cluster_name = coalesce(var.iam_role_cluster_name, format("%s-%s", local.name, var.iam_role_cluster_class))
}

################################################################################
# Data Sources
################################################################################

data "aws_iam_policy_document" "iam_role_cluster_assume_policy" {
  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRole",
      "sts:TagSession"
    ]

    principals {
      type = "Service"

      identifiers = [
        "eks.amazonaws.com"
      ]
    }
  }
}

################################################################################
# Resources
################################################################################

resource "aws_iam_role" "cluster" {
  name                  = local.iam_role_cluster_name
  path                  = var.iam_role_cluster_path
  description           = var.iam_role_cluster_description
  assume_role_policy    = data.aws_iam_policy_document.iam_role_cluster_assume_policy.json
  force_detach_policies = var.iam_role_cluster_force_detach
}

################################################################################
# Outputs
################################################################################

output "iam_role_cluster_path" {
  value = var.iam_role_cluster_path
}

output "iam_role_cluster_force_detach" {
  value = var.iam_role_cluster_force_detach
}

################################################################################

output "iam_role_cluster_id" {
  value = aws_iam_role.cluster.unique_id
}

output "iam_role_cluster_arn" {
  value = aws_iam_role.cluster.arn
}

output "iam_role_cluster_name" {
  value = aws_iam_role.cluster.name
}

output "iam_role_cluster_created" {
  value = aws_iam_role.cluster.create_date
}

output "iam_role_cluster_description" {
  value = aws_iam_role.cluster.description
}

output "iam_role_cluster_assume_policy" {
  value = aws_iam_role.cluster.assume_role_policy
}

################################################################################
