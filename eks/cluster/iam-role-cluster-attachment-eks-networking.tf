################################################################################
# Optional Variables
################################################################################

variable "iam_role_cluster_attachment_eks_networking_policy_name" {
  type        = string
  default     = "AmazonEKSNetworkingPolicy"
  description = "The name of the built-in EKS networking IAM policy to attach to the EKS cluster IAM role."
}

################################################################################
# Locals
################################################################################

locals {
  iam_role_cluster_attachment_eks_networking_policy_arn = format("arn:aws:iam::aws:policy/%s", var.iam_role_cluster_attachment_eks_networking_policy_name)
}

################################################################################
# Resources
################################################################################

resource "aws_iam_role_policy_attachment" "cluster_eks_networking" {
  count = var.auto ? 1 : 0

  role       = aws_iam_role.cluster.name
  policy_arn = local.iam_role_cluster_attachment_eks_networking_policy_arn
}

################################################################################
# Outputs
################################################################################

output "iam_role_cluster_attachment_eks_networking_role" {
  value = length(aws_iam_role_policy_attachment.cluster_eks_networking) == 1 ? aws_iam_role_policy_attachment.cluster_eks_networking[0].role : null
}

output "iam_role_cluster_attachment_eks_networking_policy_arn" {
  value = length(aws_iam_role_policy_attachment.cluster_eks_networking) == 1 ? aws_iam_role_policy_attachment.cluster_eks_networking[0].policy_arn : null
}

################################################################################
