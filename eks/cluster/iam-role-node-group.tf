################################################################################
# Optional Variables
################################################################################

variable "iam_role_node_group_name" {
  type        = string
  default     = null
  description = "The full name of the EKS auto-mode node groups IAM role."
}

variable "iam_role_node_group_path" {
  type        = string
  default     = "/"
  description = "The IAM path to the EKS auto-mode node groups IAM role."
}

variable "iam_role_node_group_class" {
  type        = string
  default     = "node-groups"
  description = "The identifier for the EKS auto-mode node groups IAM role. This is appended to the EKS clusters name. Ignored if 'iam_role_node_group_name' is set."
}

variable "iam_role_node_group_description" {
  type        = string
  default     = "Managed by Terraform"
  description = "The description of the EKS auto-mode node groups IAM role."
}

variable "iam_role_node_group_force_detach" {
  type        = bool
  default     = false
  description = "Specifies to force detaching any policies the EKS auto-mode node groups IAM role has before destroying it."
}

################################################################################
# Locals
################################################################################

locals {
  iam_role_node_group_name = coalesce(var.iam_role_node_group_name, format("%s-%s", local.name, var.iam_role_node_group_class))
}

################################################################################
# Data Sources
################################################################################

data "aws_iam_policy_document" "iam_role_node_group_assume_policy" {
  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRole"
    ]

    principals {
      type = "Service"

      identifiers = [
        "ec2.amazonaws.com"
      ]
    }
  }
}

################################################################################
# Resources
################################################################################

resource "aws_iam_role" "node_group" {
  count                 = local.node_group_enabled ? 1 : 0
  name                  = local.iam_role_node_group_name
  path                  = var.iam_role_node_group_path
  description           = var.iam_role_node_group_description
  assume_role_policy    = data.aws_iam_policy_document.iam_role_node_group_assume_policy.json
  force_detach_policies = var.iam_role_node_group_force_detach
}

################################################################################
# Outputs
################################################################################

output "iam_role_node_group_path" {
  value = var.iam_role_node_group_path
}

output "iam_role_node_group_force_detach" {
  value = var.iam_role_node_group_force_detach
}

################################################################################

output "iam_role_node_group_id" {
  value = length(aws_iam_role.node_group) == 1 ? aws_iam_role.node_group[0].unique_id : null
}

output "iam_role_node_group_arn" {
  value = length(aws_iam_role.node_group) == 1 ? aws_iam_role.node_group[0].arn : null
}

output "iam_role_node_group_name" {
  value = length(aws_iam_role.node_group) == 1 ? aws_iam_role.node_group[0].name : null
}

output "iam_role_node_group_created" {
  value = length(aws_iam_role.node_group) == 1 ? aws_iam_role.node_group[0].create_date : null
}

output "iam_role_node_group_description" {
  value = length(aws_iam_role.node_group) == 1 ? aws_iam_role.node_group[0].description : null
}

output "iam_role_node_group_assume_policy" {
  value = length(aws_iam_role.node_group) == 1 ? aws_iam_role.node_group[0].assume_role_policy : null
}

################################################################################

