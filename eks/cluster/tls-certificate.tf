################################################################################
# Optional Variables
################################################################################

variable "tls_certificate_verify" {
  type        = bool
  default     = true
  description = "Whether to verify the certificate chain while parsing it or not."
}

################################################################################
# Data Sources
################################################################################

data "tls_certificate" "scope" {
  url          = aws_eks_cluster.scope.identity[0].oidc[0].issuer
  verify_chain = var.tls_certificate_verify
}

################################################################################
# Outputs
################################################################################

output "tls_certificate_verify" {
  value = var.tls_certificate_verify
}

################################################################################

output "tls_certificate_list" {
  value = data.tls_certificate.scope.certificates
}

################################################################################
