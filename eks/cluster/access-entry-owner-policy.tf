################################################################################
# Optional Variables
################################################################################

variable "access_entry_owner_policy_map" {
  description = "A map of policies and the attachment details for the EKS cluster owner access entry. Ignored unless 'authentication_owner' is 'true'."

  type = map(object({
    arn        = optional(string)
    namespaces = optional(set(string))
  }))

  default = {
    "AmazonEKSClusterAdminPolicy" = {}
  }

  validation {
    condition     = var.access_entry_owner_policy_map != null && length(var.access_entry_owner_policy_map) > 0
    error_message = "The map of policies for the EKS cluster owner access entry must have at least one policy specified."
  }

  validation {
    condition     = alltrue([for policy in var.access_entry_owner_policy_map : policy.namespaces != null ? length(policy.namespaces) > 0 : true])
    error_message = "Every policy namespace restriction for the EKS cluster owner must be 'null' or a set with at least one namespace."
  }
}

################################################################################
# Locals
################################################################################

locals {
  access_entry_owner_policy_map = var.authentication_owner ? {
    for key, value in var.access_entry_owner_policy_map : key => {
      "arn"        = coalesce(value.arn, format("arn:aws:eks::aws:cluster-access-policy/%s", key))
      "namespaces" = value.namespaces
    }
  } : {}
}

################################################################################
# Resources
################################################################################

resource "aws_eks_access_policy_association" "owner" {
  for_each      = local.access_entry_owner_policy_map
  policy_arn    = each.value.arn
  cluster_name  = aws_eks_access_entry.owner[0].cluster_name
  principal_arn = aws_eks_access_entry.owner[0].principal_arn

  dynamic "access_scope" {
    for_each = each.value.namespaces == null ? [null] : []

    content {
      type = "cluster"
    }
  }

  dynamic "access_scope" {
    for_each = each.value.namespaces != null ? [null] : []

    content {
      type       = "namespace"
      namespaces = each.value.namespaces
    }
  }
}

################################################################################
# Outputs
################################################################################

output "access_entry_owner_policy_map" {
  value = local.access_entry_owner_policy_map
}

################################################################################

output "access_entry_owner_policy_arns" {
  value = {
    for key, value in aws_eks_access_policy_association.owner : key => value.policy_arn
  }
}

output "access_entry_owner_policy_created" {
  value = {
    for key, value in aws_eks_access_policy_association.owner : key => value.associated_at
  }
}

output "access_entry_owner_policy_clusters" {
  value = {
    for key, value in aws_eks_access_policy_association.owner : key => value.cluster_name
  }
}

output "access_entry_owner_policy_modified" {
  value = {
    for key, value in aws_eks_access_policy_association.owner : key => value.modified_at
  }
}

output "access_entry_owner_policy_principals" {
  value = {
    for key, value in aws_eks_access_policy_association.owner : key => value.principal_arn
  }
}

################################################################################
