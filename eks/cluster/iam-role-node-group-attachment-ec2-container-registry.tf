################################################################################
# Optional Variables
################################################################################

variable "iam_role_node_group_attachment_ec2_container_registry_policy_name" {
  type        = string
  default     = "AmazonEC2ContainerRegistryPullOnly"
  description = "The name of the built-in EC2 container registry IAM policy to attach to the EKS node groups IAM role."
}

variable "iam_role_node_group_attachment_ec2_container_registry_policy_enabled" {
  type        = bool
  default     = true
  description = "Should the built-in EC2 container registry IAM policy be attached to the EKS node groups IAM role."
}

################################################################################
# Locals
################################################################################

locals {
  iam_role_node_group_attachment_ec2_container_registry_policy_arn = format("arn:aws:iam::aws:policy/%s", var.iam_role_node_group_attachment_ec2_container_registry_policy_name)
}

################################################################################
# Resources
################################################################################

resource "aws_iam_role_policy_attachment" "node_group_ec2_container_registry" {
  count      = local.node_group_enabled && var.iam_role_node_group_attachment_ec2_container_registry_policy_enabled ? 1 : 0
  role       = aws_iam_role.node_group[0].name
  policy_arn = local.iam_role_node_group_attachment_ec2_container_registry_policy_arn
}

################################################################################
# Outputs
################################################################################

output "iam_role_node_group_attachment_ec2_container_registry_policy_name" {
  value = var.iam_role_node_group_attachment_ec2_container_registry_policy_name
}

output "iam_role_node_group_attachment_ec2_container_registry_policy_enabled" {
  value = var.iam_role_node_group_attachment_ec2_container_registry_policy_enabled
}

################################################################################

output "iam_role_node_group_attachment_ec2_container_registry_role" {
  value = length(aws_iam_role_policy_attachment.node_group_ec2_container_registry) == 1 ? aws_iam_role_policy_attachment.node_group_ec2_container_registry[0].role : null
}

output "iam_role_node_group_attachment_ec2_container_registry_policy_arn" {
  value = length(aws_iam_role_policy_attachment.node_group_ec2_container_registry) == 1 ? aws_iam_role_policy_attachment.node_group_ec2_container_registry[0].policy_arn : null
}

################################################################################
