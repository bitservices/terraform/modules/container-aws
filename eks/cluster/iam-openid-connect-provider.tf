################################################################################
# Optional Variables
################################################################################

variable "iam_openid_connect_provider_audiences" {
  type        = list(string)
  description = "A list of client IDs. When a mobile or web app registers with an OpenID Connect provider, they establish a value that identifies the application."

  default = [
    "sts.amazonaws.com"
  ]
}

################################################################################
# Resources
################################################################################

resource "aws_iam_openid_connect_provider" "scope" {
  url             = aws_eks_cluster.scope.identity[0].oidc[0].issuer
  client_id_list  = var.iam_openid_connect_provider_audiences
  thumbprint_list = tolist([data.tls_certificate.scope.certificates[0].sha1_fingerprint])

  tags = {
    "URL"     = aws_eks_cluster.scope.identity[0].oidc[0].issuer
    "VPC"     = var.vpc
    "Owner"   = var.owner
    "Region"  = data.aws_region.scope.name
    "Cluster" = aws_eks_cluster.scope.id
    "Company" = var.company
  }
}

################################################################################
# Outputs
################################################################################

output "iam_openid_connect_provider_arn" {
  value = aws_iam_openid_connect_provider.scope.arn
}

output "iam_openid_connect_provider_url" {
  value = aws_iam_openid_connect_provider.scope.url
}

output "iam_openid_connect_provider_tags" {
  value = aws_iam_openid_connect_provider.scope.tags_all
}

output "iam_openid_connect_provider_audiences" {
  value = aws_iam_openid_connect_provider.scope.client_id_list
}

output "iam_openid_connect_provider_thumbprints" {
  value = aws_iam_openid_connect_provider.scope.thumbprint_list
}

################################################################################
