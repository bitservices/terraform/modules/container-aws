################################################################################
# Required Variables
################################################################################

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "auto" {
  type        = bool
  default     = false
  description = "Use EKS auto mode?"
}

variable "ipv6" {
  type        = bool
  default     = true
  description = "Create an IPv6 based EKS cluster."
}

variable "class" {
  type        = string
  default     = "eks"
  description = "Identifier for this EKS cluster within its VPC."
}

variable "public" {
  type        = bool
  default     = false
  description = "Whether the Amazon EKS public API server endpoint is enabled."
}

variable "release" {
  type        = string
  default     = "1.32"
  description = "Desired Kubernetes version."
}

variable "bootstrap_addons" {
  type        = bool
  default     = false
  description = "Should bare-minimum add-ons for the cluster to work be installed? If 'false' certain add-ons must be manually installed straight after cluster creation. Ignored if 'auto' is 'true'."
}

variable "extended_support" {
  type        = bool
  default     = false
  description = "Enable cluster to enter extended support (more expensive) rather than being automatically upgraded at the end of standard support."
}

variable "pod_security_groups" {
  type        = bool
  default     = false
  description = "Should security groups for pods be enabled?"
}

################################################################################

variable "api_private_allow_vpc" {
  type        = bool
  default     = true
  description = "Allow VPC access to the API when 'public' is 'false'."
}

variable "api_public_ipv4_cidrs" {
  type        = list(string)
  description = "List of IPv4 CIDR ranges that have Kubernetes API access when 'public' is 'true'."

  default = [
    "0.0.0.0/0"
  ]
}

################################################################################

variable "authentication_owner" {
  type        = bool
  default     = false
  description = "Allow the cluster creator administrative privileges to the cluster."
}

variable "authentication_configmap" {
  type        = bool
  default     = false
  description = "Allow authentication via the AWS authentication configmap."
}

################################################################################

variable "node_group_system" {
  type        = bool
  default     = false
  description = "Enable the system node group. Ignored unless 'auto' is 'true'."
}

variable "node_group_general" {
  type        = bool
  default     = true
  description = "Enable the general purpose node group. Ignored unless 'auto' is 'true'."
}

################################################################################
# Locals
################################################################################

locals {
  name                  = format("%s-%s", var.vpc, var.class)
  node_group_list       = local.node_group_system || local.node_group_general ? compact([local.node_group_general ? "general-purpose" : null, local.node_group_system ? "system" : null]) : null
  bootstrap_addons      = var.auto ? false : var.bootstrap_addons
  node_group_system     = var.auto && var.node_group_system
  node_group_enabled    = local.node_group_list != null
  node_group_general    = var.auto && var.node_group_general
  api_private_allow_vpc = var.public ? false : var.api_private_allow_vpc
  api_public_ipv4_cidrs = var.public ? var.api_public_ipv4_cidrs : null
}

################################################################################
# Resources
################################################################################

resource "aws_eks_cluster" "scope" {
  name                          = local.name
  version                       = var.release
  role_arn                      = aws_iam_role.cluster.arn
  bootstrap_self_managed_addons = local.bootstrap_addons

  tags = {
    "VPC"               = var.vpc
    "Auto"              = var.auto ? "Enabled" : "Disabled"
    "IPv6"              = var.ipv6 ? "Enabled" : "Disabled"
    "Name"              = local.name
    "Class"             = var.class
    "Owner"             = var.owner
    "Public"            = var.public ? "Enabled" : "Disabled"
    "Region"            = data.aws_region.scope.name
    "Company"           = var.company
    "Release"           = var.release
    "PodSecurityGroups" = var.pod_security_groups ? "Enabled" : "Disabled"
  }

  access_config {
    authentication_mode                         = var.authentication_configmap ? "API_AND_CONFIG_MAP" : "API"
    bootstrap_cluster_creator_admin_permissions = false
  }

  compute_config {
    enabled       = var.auto
    node_pools    = local.node_group_list
    node_role_arn = local.node_group_enabled ? aws_iam_role.node_group[0].arn : null
  }

  kubernetes_network_config {
    ip_family = var.ipv6 ? "ipv6" : "ipv4"

    elastic_load_balancing {
      enabled = var.auto
    }
  }

  storage_config {
    block_storage {
      enabled = var.auto
    }
  }

  upgrade_policy {
    support_type = var.extended_support ? "EXTENDED" : "STANDARD"
  }

  vpc_config {
    subnet_ids              = local.subnet_ids
    security_group_ids      = concat(aws_security_group.api.*.id, var.security_group_extra_ids, data.aws_security_group.extra.*.id)
    public_access_cidrs     = local.api_public_ipv4_cidrs
    endpoint_public_access  = var.public
    endpoint_private_access = true
  }

  depends_on = [
    aws_iam_role_policy_attachment.cluster_eks_cluster,
    aws_iam_role_policy_attachment.cluster_eks_compute,
    aws_iam_role_policy_attachment.cluster_eks_block_storage,
    aws_iam_role_policy_attachment.cluster_eks_load_balancing,
    aws_iam_role_policy_attachment.cluster_eks_networking,
    aws_iam_role_policy_attachment.cluster_eks_vpc_resource_controller,
    aws_iam_role_policy_attachment.node_group_ec2_container_registry,
    aws_iam_role_policy_attachment.node_group_eks_worker_node,
    aws_iam_role_policy.node_group_extra
  ]
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "auto" {
  value = var.auto
}

output "ipv6" {
  value = var.ipv6
}

output "class" {
  value = var.class
}

output "public" {
  value = var.public
}

output "release" {
  value = var.release
}

output "extended_support" {
  value = var.extended_support
}

output "pod_security_groups" {
  value = var.pod_security_groups
}

################################################################################

output "api_private_allow_vpc" {
  value = local.api_private_allow_vpc
}

output "api_public_ipv4_cidrs" {
  value = local.api_public_ipv4_cidrs
}

################################################################################

output "authentication_owner" {
  value = var.authentication_owner
}

output "authentication_configmap" {
  value = var.authentication_configmap
}

################################################################################

output "node_group_list" {
  value = local.node_group_list
}

output "node_group_system" {
  value = local.node_group_system
}

output "node_group_enabled" {
  value = local.node_group_general
}

output "node_group_general" {
  value = local.node_group_general
}

################################################################################

output "ca" {
  value = aws_eks_cluster.scope.certificate_authority
}

output "arn" {
  value = aws_eks_cluster.scope.arn

  depends_on = [
    aws_eks_access_policy_association.owner
  ]
}

output "name" {
  value = aws_eks_cluster.scope.id

  depends_on = [
    aws_eks_access_policy_association.owner
  ]
}

output "tags" {
  value = aws_eks_cluster.scope.tags_all
}

output "status" {
  value = aws_eks_cluster.scope.status
}

output "created" {
  value = aws_eks_cluster.scope.created_at
}

output "endpoint" {
  value = aws_eks_cluster.scope.endpoint

  depends_on = [
    aws_eks_access_policy_association.owner
  ]
}

output "identity" {
  value = aws_eks_cluster.scope.identity
}

output "platform" {
  value = aws_eks_cluster.scope.platform_version
}

output "bootstrap_addons" {
  value = aws_eks_cluster.scope.bootstrap_self_managed_addons
}

################################################################################

output "vpc_security_group_id" {
  value = aws_eks_cluster.scope.vpc_config[0].cluster_security_group_id
}

################################################################################
