<!----------------------------------------------------------------------------->

# eks/cluster

#### Manage Elastic [Kubernetes] Service ([EKS]) clusters

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/container/aws//eks/cluster`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_eks_cluster" {
  source  = "gitlab.com/bitservices/container/aws//eks/cluster"
  vpc     = "sandpit01"
  owner   = "terraform@bitservices.io"
  company = "BITServices Ltd"
}
```

<!----------------------------------------------------------------------------->

[EKS]:        https://aws.amazon.com/eks/
[Kubernetes]: https://kubernetes.io/

<!----------------------------------------------------------------------------->
