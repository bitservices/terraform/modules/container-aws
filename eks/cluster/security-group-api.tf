################################################################################
# Optional Variables
################################################################################

variable "security_group_api_class" {
  type        = string
  default     = "api"
  description = "This forms the name of the EKS API access security group. It is appended to the EKS clusters name."
}

variable "security_group_api_description" {
  type        = string
  default     = "Managed by Terraform"
  description = "The EKS API access security group security group description."
}

################################################################################

variable "security_group_api_rule_flow" {
  type        = string
  default     = "ingress"
  description = "The type of rule being created. Can be 'ingress' or 'egress'."
}

variable "security_group_api_rule_port" {
  type        = number
  default     = 443
  description = "The start port (or ICMP type number if protocol is 'icmp')."
}

variable "security_group_api_rule_to_port" {
  type        = number
  default     = null
  description = "The end port (or ICMP code if protocol is 'icmp')."
}

variable "security_group_api_rule_protocol" {
  type        = string
  default     = "tcp"
  description = "The protocol. If not 'icmp', 'tcp', 'udp', or 'all' use the protocol number."
}

variable "security_group_api_rule_description" {
  type        = string
  default     = null
  description = "The description of this security group rule."
}

################################################################################
# Locals
################################################################################

locals {
  security_group_api_name             = format("%s-%s", local.name, var.security_group_api_class)
  security_group_api_rule_to_port     = coalesce(var.security_group_api_rule_to_port, var.security_group_api_rule_port)
  security_group_api_rule_description = coalesce(var.security_group_api_rule_description, var.security_group_api_description)
}

################################################################################
# Resources
################################################################################

resource "aws_security_group" "api" {
  count       = local.api_private_allow_vpc ? 1 : 0
  name        = local.security_group_api_name
  vpc_id      = data.aws_vpc.scope.id
  description = var.security_group_api_description

  tags = {
    "VPC"     = var.vpc
    "Name"    = local.security_group_api_name
    "Class"   = var.class
    "Owner"   = var.owner
    "Region"  = data.aws_region.scope.name
    "Cluster" = local.name
    "Company" = var.company
  }
}

################################################################################

resource "aws_security_group_rule" "api" {
  count             = local.api_private_allow_vpc ? 1 : 0
  type              = var.security_group_api_rule_flow
  to_port           = local.security_group_api_rule_to_port
  protocol          = var.security_group_api_rule_protocol
  from_port         = var.security_group_api_rule_port
  cidr_blocks       = tolist([data.aws_vpc.scope.cidr_block])
  description       = local.security_group_api_rule_description
  ipv6_cidr_blocks  = tolist([data.aws_vpc.scope.ipv6_cidr_block])
  security_group_id = aws_security_group.api[0].id
}

################################################################################
# Outputs
################################################################################

output "security_group_api_class" {
  value = var.security_group_api_class
}

################################################################################

output "security_group_api_id" {
  value = length(aws_security_group.api) == 1 ? aws_security_group.api[0].id : null
}

output "security_group_api_name" {
  value = length(aws_security_group.api) == 1 ? aws_security_group.api[0].name : null
}

output "security_group_api_tags" {
  value = length(aws_security_group.api) == 1 ? aws_security_group.api[0].tags_all : null
}

output "security_group_api_account_id" {
  value = length(aws_security_group.api) == 1 ? aws_security_group.api[0].owner_id : null
}

output "security_group_api_description" {
  value = length(aws_security_group.api) == 1 ? aws_security_group.api[0].description : null
}

################################################################################

output "security_group_api_rule_id" {
  value = length(aws_security_group_rule.api) == 1 ? aws_security_group_rule.api[0].id : null
}

output "security_group_api_rule_flow" {
  value = length(aws_security_group_rule.api) == 1 ? aws_security_group_rule.api[0].type : null
}

output "security_group_api_rule_port" {
  value = length(aws_security_group_rule.api) == 1 ? aws_security_group_rule.api[0].from_port : null
}

output "security_group_api_rule_to_port" {
  value = length(aws_security_group_rule.api) == 1 ? aws_security_group_rule.api[0].to_port : null
}

output "security_group_api_rule_protocol" {
  value = length(aws_security_group_rule.api) == 1 ? aws_security_group_rule.api[0].protocol : null
}

output "security_group_api_rule_description" {
  value = length(aws_security_group_rule.api) == 1 ? aws_security_group_rule.api[0].description : null
}

################################################################################
