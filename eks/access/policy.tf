################################################################################
# Optional Variables
################################################################################

variable "policy_map" {
  default     = {}
  description = "A map of policies and the attachment details for this access entry."

  type = map(object({
    arn        = optional(string)
    namespaces = optional(set(string))
  }))

  validation {
    condition     = alltrue([for policy in var.policy_map : policy.namespaces != null ? length(policy.namespaces) > 0 : true])
    error_message = "Every policy namespace restriction must be 'null' or a set with at least one namespace."
  }
}

################################################################################
# Locals
################################################################################

locals {
  policy_map = {
    for key, value in var.policy_map : key => {
      "arn"        = coalesce(value.arn, format("arn:aws:eks::aws:cluster-access-policy/%s", key))
      "namespaces" = value.namespaces
    }
  }
}

################################################################################
# Resources
################################################################################

resource "aws_eks_access_policy_association" "scope" {
  for_each      = local.policy_map
  policy_arn    = each.value.arn
  cluster_name  = aws_eks_access_entry.scope.cluster_name
  principal_arn = aws_eks_access_entry.scope.principal_arn

  dynamic "access_scope" {
    for_each = each.value.namespaces == null ? [null] : []

    content {
      type = "cluster"
    }
  }

  dynamic "access_scope" {
    for_each = each.value.namespaces != null ? [null] : []

    content {
      type       = "namespace"
      namespaces = each.value.namespaces
    }
  }
}

################################################################################
# Outputs
################################################################################

output "policy_map" {
  value = local.policy_map
}

################################################################################

output "policy_arns" {
  value = {
    for key, value in aws_eks_access_policy_association.scope : key => value.policy_arn
  }
}

output "policy_created" {
  value = {
    for key, value in aws_eks_access_policy_association.scope : key => value.associated_at
  }
}

output "policy_clusters" {
  value = {
    for key, value in aws_eks_access_policy_association.scope : key => value.cluster_name
  }
}

output "policy_modified" {
  value = {
    for key, value in aws_eks_access_policy_association.scope : key => value.modified_at
  }
}

output "policy_principals" {
  value = {
    for key, value in aws_eks_access_policy_association.scope : key => value.principal_arn
  }
}

################################################################################
