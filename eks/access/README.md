<!----------------------------------------------------------------------------->

# eks/access

#### Manage Elastic [Kubernetes] Service ([EKS]) access entries

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/container/aws//eks/access`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "vpc"     { default = "sandpit01"                }
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_iam_role" {
  source = "gitlab.com/bitservices/identity/aws//role/detached"
  name   = "MyRole"
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::012345678901:user/username"
      },
      "Action": "sts:AssumeRole",
      "Condition": {}
    }
  ]
}
POLICY
}

module "my_eks_cluster" {
  source  = "gitlab.com/bitservices/container/aws//eks/cluster"
  vpc     = var.vpc
  owner   = var.owner
  company = var.company
}

module "my_eks_access" {
  source    = "gitlab.com/bitservices/container/aws//eks/access"
  vpc       = var.vpc
  owner     = var.owner
  company   = var.company
  cluster   = module.my_eks_cluster.name
  principal = module.my_iam_role.arn
}
```

<!----------------------------------------------------------------------------->

[EKS]:        https://aws.amazon.com/eks/
[Kubernetes]: https://kubernetes.io/

<!----------------------------------------------------------------------------->
