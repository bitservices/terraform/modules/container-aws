################################################################################
# Required Variables
################################################################################

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################

variable "cluster" {
  type        = string
  description = "Name of the EKS Cluster."
}

variable "principal" {
  type        = string
  description = "The IAM principal ARN to grant access to in this access entry."
}

################################################################################
# Optional Variables
################################################################################

variable "type" {
  type        = string
  default     = "STANDARD"
  description = "The access entry type. Must be either: 'STANDARD', 'EC2_LINUX', 'EC2_WINDOWS' or 'FARGATE_LINUX'."

  validation {
    condition     = contains(["STANDARD", "EC2_LINUX", "EC2_WINDOWS", "FARGATE_LINUX"], var.type)
    error_message = "The access entry type must be either: 'STANDARD', 'EC2_LINUX', 'EC2_WINDOWS' or 'FARGATE_LINUX'."
  }
}

variable "groups" {
  type        = set(string)
  default     = null
  description = "List of string which can optionally specify the Kubernetes groups the principal would belong to when creating an access entry."
}

variable "username" {
  type        = string
  default     = null
  description = "Override the default username given to the access entry."
}

################################################################################
# Locals
################################################################################

locals {
  groups = var.groups != null ? tolist(var.groups) : null
}

################################################################################
# Resources
################################################################################

resource "aws_eks_access_entry" "scope" {
  type              = var.type
  user_name         = var.username
  cluster_name      = var.cluster
  principal_arn     = var.principal
  kubernetes_groups = local.groups

  tags = {
    "VPC"     = var.vpc
    "Owner"   = var.owner
    "Region"  = data.aws_region.scope.name
    "Cluster" = var.cluster
    "Company" = var.company
  }
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "id" {
  value = aws_eks_access_entry.scope.id
}

output "arn" {
  value = aws_eks_access_entry.scope.access_entry_arn
}

output "tags" {
  value = aws_eks_access_entry.scope.tags_all
}

output "type" {
  value = aws_eks_access_entry.scope.type
}

output "groups" {
  value = aws_eks_access_entry.scope.kubernetes_groups
}

output "cluster" {
  value = aws_eks_access_entry.scope.cluster_name
}

output "created" {
  value = aws_eks_access_entry.scope.created_at
}

output "modified" {
  value = aws_eks_access_entry.scope.modified_at
}

output "username" {
  value = aws_eks_access_entry.scope.user_name
}

output "principal" {
  value = aws_eks_access_entry.scope.principal_arn
}

################################################################################
