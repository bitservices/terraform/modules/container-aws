################################################################################
# Optional Variables
################################################################################

variable "iam_role_name" {
  type        = string
  default     = null
  description = "The full name of the EKS node groups IAM role."
}

variable "iam_role_path" {
  type        = string
  default     = "/"
  description = "The IAM path to this EKS node groups role."
}

variable "iam_role_class" {
  type        = string
  default     = "node-groups"
  description = "The identifier for this EKS node groups IAM role. This is appended to the EKS node group name. Ignored if 'iam_role_name' is set."
}

variable "iam_role_description" {
  type        = string
  default     = "Managed by Terraform"
  description = "The description of this EKS node groups IAM role."
}

variable "iam_role_force_detach" {
  type        = bool
  default     = false
  description = "Specifies to force detaching any policies this EKS node groups IAM role has before destroying it."
}

################################################################################
# Locals
################################################################################

locals {
  iam_role_name = coalesce(var.iam_role_name, format("%s-%s", local.name, var.iam_role_class))
}

################################################################################
# Data Sources
################################################################################

data "aws_iam_policy_document" "iam_role_assume_policy" {
  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRole"
    ]

    principals {
      type = "Service"

      identifiers = [
        "ec2.amazonaws.com"
      ]
    }
  }
}

################################################################################
# Resources
################################################################################

resource "aws_iam_role" "scope" {
  name                  = local.iam_role_name
  path                  = var.iam_role_path
  description           = var.iam_role_description
  assume_role_policy    = data.aws_iam_policy_document.iam_role_assume_policy.json
  force_detach_policies = var.iam_role_force_detach
}

################################################################################
# Outputs
################################################################################

output "iam_role_path" {
  value = var.iam_role_path
}

output "iam_role_force_detach" {
  value = var.iam_role_force_detach
}

################################################################################

output "iam_role_id" {
  value = aws_iam_role.scope.unique_id
}

output "iam_role_arn" {
  value = aws_iam_role.scope.arn
}

output "iam_role_name" {
  value = aws_iam_role.scope.name
}

output "iam_role_created" {
  value = aws_iam_role.scope.create_date
}

output "iam_role_description" {
  value = aws_iam_role.scope.description
}

output "iam_role_assume_policy" {
  value = aws_iam_role.scope.assume_role_policy
}

################################################################################
