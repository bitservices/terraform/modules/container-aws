################################################################################
# Optional Variables
################################################################################

variable "iam_role_attachment_eks_worker_node_policy_name" {
  type        = string
  default     = "AmazonEKSWorkerNodePolicy"
  description = "The name of the built-in EKS worker node IAM policy to attach to the EKS node groups IAM role."
}

################################################################################
# Locals
################################################################################

locals {
  iam_role_attachment_eks_worker_node_policy_arn = format("arn:aws:iam::aws:policy/%s", var.iam_role_attachment_eks_worker_node_policy_name)
}

################################################################################
# Resources
################################################################################

resource "aws_iam_role_policy_attachment" "eks_worker_node" {
  role       = aws_iam_role.scope.name
  policy_arn = local.iam_role_attachment_eks_worker_node_policy_arn
}

################################################################################
# Outputs
################################################################################

output "iam_role_attachment_eks_worker_node_policy_name" {
  value = var.iam_role_attachment_eks_worker_node_policy_name
}

################################################################################

output "iam_role_attachment_eks_worker_node_role" {
  value = aws_iam_role_policy_attachment.eks_worker_node.role
}

output "iam_role_attachment_eks_worker_node_policy_arn" {
  value = aws_iam_role_policy_attachment.eks_worker_node.policy_arn
}

################################################################################
