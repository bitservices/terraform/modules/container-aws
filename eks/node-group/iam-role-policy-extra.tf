################################################################################
# Optional Variables
################################################################################

variable "iam_role_policy_extra_name" {
  type        = string
  default     = "extra"
  description = "The name of the extra IAM role in-line policy to create."
}

variable "iam_role_policy_extra_policy" {
  type        = string
  default     = null
  description = "The extra policy document in JSON format. Ignored if 'iam_role_policy_extra_enabled' is 'false'."
}

variable "iam_role_policy_extra_enabled" {
  type        = bool
  default     = false
  description = "Should the extra IAM role in-line policy be created? If 'true', 'iam_role_policy_extra_policy' must be set."
}

################################################################################
# Resources
################################################################################

resource "aws_iam_role_policy" "extra" {
  count  = var.iam_role_policy_extra_enabled ? 1 : 0
  name   = var.iam_role_policy_extra_name
  role   = aws_iam_role.scope.arn
  policy = var.iam_role_policy_extra_policy
}

################################################################################
# Outputs
################################################################################

output "iam_role_policy_extra_enabled" {
  value = var.iam_role_policy_extra_enabled
}

################################################################################

output "iam_role_policy_extra_id" {
  value = length(aws_iam_role_policy.extra) == 1 ? aws_iam_role_policy.extra[0].id : null
}

output "iam_role_policy_extra_name" {
  value = length(aws_iam_role_policy.extra) == 1 ? aws_iam_role_policy.extra[0].name : null
}

output "iam_role_policy_extra_role" {
  value = length(aws_iam_role_policy.extra) == 1 ? aws_iam_role_policy.extra[0].role : null
}

output "iam_role_policy_extra_policy" {
  value = length(aws_iam_role_policy.extra) == 1 ? aws_iam_role_policy.extra[0].policy : null
}

################################################################################
