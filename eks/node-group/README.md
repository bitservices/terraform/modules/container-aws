<!----------------------------------------------------------------------------->

# eks/node-group

#### Manage Elastic [Kubernetes] Service ([EKS]) node groups

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/container/aws//eks/node-group`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "vpc"     { default = "sandpit01"                }
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_eks_cluster" {
  source  = "gitlab.com/bitservices/container/aws//eks/cluster"
  vpc     = var.vpc
  owner   = var.owner
  company = var.company
}

module "my_eks_node_group_default_a" {
  source      = "gitlab.com/bitservices/container/aws//eks/node-group"
  vpc         = var.vpc
  class       = "default-a"
  owner       = var.owner
  company     = var.company
  cluster     = module.my_eks_cluster.name
  release     = module.my_eks_cluster.release
  subnet_zone = "a"
}

module "my_eks_node_group_default_b" {
  source      = "gitlab.com/bitservices/container/aws//eks/node-group"
  vpc         = var.vpc
  class       = "default-b"
  owner       = var.owner
  company     = var.company
  cluster     = module.my_eks_cluster.name
  release     = module.my_eks_cluster.release
  subnet_zone = "b"
}
```

<!----------------------------------------------------------------------------->

[EKS]:        https://aws.amazon.com/eks/
[Kubernetes]: https://kubernetes.io/

<!----------------------------------------------------------------------------->
