################################################################################
# Optional Variables
################################################################################

variable "access_entry_type" {
  type        = string
  default     = "EC2_LINUX"
  description = "The access entry type for this node group to be given access to the cluster."
}

################################################################################
# Resources
################################################################################

resource "aws_eks_access_entry" "scope" {
  type          = var.access_entry_type
  cluster_name  = var.cluster
  principal_arn = aws_iam_role.scope.arn

  tags = {
    "VPC"       = var.vpc
    "Owner"     = var.owner
    "Region"    = data.aws_region.scope.name
    "Cluster"   = var.cluster
    "Company"   = var.company
    "NodeGroup" = local.name
  }
}

################################################################################
# Outputs
################################################################################

output "access_entry_id" {
  value = aws_eks_access_entry.scope.id
}

output "access_entry_arn" {
  value = aws_eks_access_entry.scope.access_entry_arn
}

output "access_entry_tags" {
  value = aws_eks_access_entry.scope.tags_all
}

output "access_entry_type" {
  value = aws_eks_access_entry.scope.type
}

output "access_entry_groups" {
  value = aws_eks_access_entry.scope.kubernetes_groups
}

output "access_entry_cluster" {
  value = aws_eks_access_entry.scope.cluster_name
}

output "access_entry_created" {
  value = aws_eks_access_entry.scope.created_at
}

output "access_entry_modified" {
  value = aws_eks_access_entry.scope.modified_at
}

output "access_entry_username" {
  value = aws_eks_access_entry.scope.user_name
}

output "access_entry_principal" {
  value = aws_eks_access_entry.scope.principal_arn
}

################################################################################
