################################################################################
# Required Variables
################################################################################

variable "class" {
  type        = string
  description = "Identifier for this EKS node group within its EKS cluster."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################

variable "cluster" {
  type        = string
  description = "Name of the EKS Cluster."
}

################################################################################
# Optional Variables
################################################################################

variable "ami" {
  type        = string
  default     = "BOTTLEROCKET_x86_64"
  description = "Type of Amazon Machine Image (AMI) associated with the EKS Node Group."
}

variable "types" {
  type        = list(string)
  default     = ["t3.medium"]
  description = "List of instance types associated with the EKS Node Group."
}

variable "release" {
  type        = string
  default     = null
  description = "Kubernetes version. Defaults to EKS Cluster Kubernetes version. Terraform will only perform drift detection if a configuration value is provided."
}

variable "size_gb" {
  type        = number
  default     = 20
  description = "Disk size in GiB for the nodes."
}

################################################################################

variable "scaling_max" {
  type        = number
  default     = 1
  description = "Maximum number of nodes for this EKS node group. If 'min' is higher the value for 'min' is used instead."
}

variable "scaling_min" {
  type        = number
  default     = 1
  description = "Minimum number of nodes for this EKS node group."
}

################################################################################

variable "update_force" {
  type        = bool
  default     = false
  description = "Force version update if existing pods are unable to be drained due to a pod disruption budget issue."
}

variable "update_amount" {
  type        = number
  default     = 10
  description = "The amount of nodes to update at once as either a number or percentage depending on the value of 'update_percentage'."
}

variable "update_percentage" {
  type        = bool
  default     = true
  description = "Is the value of 'update_amount' a percentage?"
}

################################################################################
# Locals
################################################################################

locals {
  name        = format("%s-%s", var.cluster, var.class)
  scaling_max = max(var.scaling_max, var.scaling_min)
}

################################################################################
# Resources
################################################################################

resource "aws_eks_node_group" "scope" {
  version              = var.release
  ami_type             = var.ami
  disk_size            = var.size_gb
  subnet_ids           = tolist([data.aws_subnet.scope.id])
  cluster_name         = var.cluster
  node_role_arn        = aws_iam_role.scope.arn
  instance_types       = var.types
  node_group_name      = local.name
  force_update_version = var.update_force

  labels = {
    "k8s.bitservices.io/vpc"     = var.vpc
    "k8s.bitservices.io/pool"    = local.name
    "k8s.bitservices.io/zone"    = data.aws_subnet.scope.tags.Zone
    "k8s.bitservices.io/region"  = data.aws_region.scope.name
    "k8s.bitservices.io/cluster" = var.cluster
  }

  tags = {
    "VPC"     = var.vpc
    "Name"    = local.name
    "Zone"    = data.aws_subnet.scope.tags.Zone
    "Class"   = var.class
    "Owner"   = var.owner
    "Region"  = data.aws_region.scope.name
    "Cluster" = var.cluster
    "Company" = var.company
    "Release" = var.release == null ? "Unmanaged" : var.release
  }

  scaling_config {
    desired_size = var.scaling_min
    max_size     = local.scaling_max
    min_size     = var.scaling_min
  }

  update_config {
    max_unavailable            = var.update_percentage ? null : var.update_amount
    max_unavailable_percentage = var.update_percentage ? var.update_amount : null
  }

  lifecycle {
    ignore_changes = [
      scaling_config[0].desired_size
    ]
  }

  depends_on = [
    aws_iam_role_policy_attachment.ec2_container_registry,
    aws_iam_role_policy_attachment.eks_worker_node,
    aws_iam_role_policy.extra
  ]
}

################################################################################
# Outputs
################################################################################

output "class" {
  value = var.class
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "ami" {
  value = var.ami
}

output "types" {
  value = var.types
}

output "release" {
  value = var.release
}

output "size_gb" {
  value = var.size_gb
}

################################################################################

output "scaling_max" {
  value = local.scaling_max
}

output "scaling_min" {
  value = var.scaling_min
}

################################################################################

output "update_force" {
  value = var.update_force
}

output "update_amount" {
  value = var.update_amount
}

output "update_percentage" {
  value = var.update_percentage
}

################################################################################

output "id" {
  value = aws_eks_node_group.scope.id
}

output "arn" {
  value = aws_eks_node_group.scope.arn
}

output "name" {
  value = aws_eks_node_group.scope.node_group_name
}

output "tags" {
  value = aws_eks_node_group.scope.tags_all
}

output "status" {
  value = aws_eks_node_group.scope.status
}

output "cluster" {
  value = aws_eks_node_group.scope.cluster_name
}

output "resources" {
  value = aws_eks_node_group.scope.resources
}

################################################################################
