################################################################################
# Required Variables
################################################################################

variable "subnet_zone" {
  type        = string
  description = "Which availbility zone should this asset be placed. Specified as a single letter."
}

################################################################################
# Optional Variables
################################################################################

variable "subnet_id" {
  type        = string
  default     = null
  description = "The ID of the subnet to which this asset will be placed. Must be specified if subnets are created in the same Terraform run."
}

variable "subnet_set" {
  type        = string
  default     = "default"
  description = "Which set of subnets to place this asset."
}

variable "subnet_tier" {
  type        = string
  default     = "private"
  description = "Which tier of subnets to place this asset."
}

################################################################################
# Data Sources
################################################################################

data "aws_subnet" "scope" {
  id     = var.subnet_id
  vpc_id = data.aws_vpc.scope.id

  tags = {
    "Set"  = var.subnet_set
    "Tier" = var.subnet_tier
    "Zone" = var.subnet_zone
  }
}

################################################################################
# Outputs
################################################################################

output "subnet_set" {
  value = var.subnet_set
}

output "subnet_tier" {
  value = var.subnet_tier
}

################################################################################

output "subnet_id" {
  value = data.aws_subnet.scope.id
}

output "subnet_name" {
  value = data.aws_subnet.scope.tags.Name
}

output "subnet_zone" {
  value = data.aws_subnet.scope.tags.Zone
}

################################################################################
