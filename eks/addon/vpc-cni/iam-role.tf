################################################################################
# Required Variables
################################################################################

variable "iam_role_kubernetes_oidc_arn" {
  type        = string
  description = "The Kubernetes OIDC ARN required for the VPC CNI addon role."
}

variable "iam_role_kubernetes_oidc_url" {
  type        = string
  description = "The Kubernetes OIDC URL required for the VPC CNI addon role."
}

################################################################################
# Optional Variables
################################################################################

variable "iam_role_name" {
  type        = string
  default     = null
  description = "The full name of the VPC CNI addon IAM role."
}

variable "iam_role_path" {
  type        = string
  default     = "/"
  description = "The IAM path to the VPC CNI addon role."
}

variable "iam_role_description" {
  type        = string
  default     = "Managed by Terraform"
  description = "The description of the VPC CNI addon IAM role."
}

variable "iam_role_force_detach" {
  type        = bool
  default     = false
  description = "Specifies to force detaching any policies the VPC CNI addon IAM role has before destroying it."
}

################################################################################

variable "iam_role_policy_namespace" {
  type        = string
  default     = "kube-system"
  description = "The namespace used by the VPC CNI addon. Required for building the VPC CNI addon IAM role assume policy."
}

variable "iam_role_policy_service_account" {
  type        = string
  default     = "aws-node"
  description = "The service account used by the VPC CNI addon. Required for building the VPC CNI addon IAM role assume policy."
}

################################################################################
# Locals
################################################################################

locals {
  iam_role_name                = coalesce(var.iam_role_name, format("%s-%s", var.cluster, var.name))
  iam_role_value               = format("system:serviceaccount:%s:%s", var.iam_role_policy_namespace, var.iam_role_policy_service_account)
  iam_role_kubernetes_oidc_sub = format("%s:sub", replace(var.iam_role_kubernetes_oidc_url, "https://", ""))
}

################################################################################
# Data Sources
################################################################################

data "aws_iam_policy_document" "iam_role_assume_policy" {
  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRoleWithWebIdentity"
    ]

    condition {
      test     = "StringEquals"
      variable = local.iam_role_kubernetes_oidc_sub

      values = [
        local.iam_role_value
      ]
    }

    principals {
      type = "Federated"

      identifiers = [
        var.iam_role_kubernetes_oidc_arn
      ]
    }
  }
}

################################################################################
# Resources
################################################################################

resource "aws_iam_role" "scope" {
  name                  = local.iam_role_name
  path                  = var.iam_role_path
  description           = var.iam_role_description
  assume_role_policy    = data.aws_iam_policy_document.iam_role_assume_policy.json
  force_detach_policies = var.iam_role_force_detach
}

################################################################################
# Outputs
################################################################################

output "iam_role_kubernetes_oidc_arn" {
  value = var.iam_role_kubernetes_oidc_arn
}

output "iam_role_kubernetes_oidc_sub" {
  value = local.iam_role_kubernetes_oidc_sub
}

output "iam_role_kubernetes_oidc_url" {
  value = var.iam_role_kubernetes_oidc_url
}

################################################################################

output "iam_role_policy_namespace" {
  value = var.iam_role_policy_namespace
}

output "iam_role_policy_service_account" {
  value = var.iam_role_policy_service_account
}

################################################################################

output "iam_role_value" {
  value = local.iam_role_value
}

################################################################################

output "iam_role_id" {
  value = aws_iam_role.scope.unique_id
}

output "iam_role_arn" {
  value = aws_iam_role.scope.arn
}

output "iam_role_name" {
  value = aws_iam_role.scope.name
}

output "iam_role_path" {
  value = aws_iam_role.scope.path
}

output "iam_role_created" {
  value = aws_iam_role.scope.create_date
}

output "iam_role_description" {
  value = aws_iam_role.scope.description
}

output "iam_role_force_detach" {
  value = aws_iam_role.scope.force_detach_policies
}

output "iam_role_assume_policy" {
  value = aws_iam_role.scope.assume_role_policy
}

################################################################################
