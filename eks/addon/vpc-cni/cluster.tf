################################################################################
# Data Sources
################################################################################

data "aws_eks_cluster" "scope" {
  name = var.cluster
}

################################################################################
# Locals
################################################################################

locals {
  cluster_ipv6 = data.aws_eks_cluster.scope.kubernetes_network_config[0].ip_family == "ipv6"
}

################################################################################
# Outputs
################################################################################

output "cluster_ipv6" {
  value = local.cluster_ipv6
}

################################################################################

output "cluster_ca" {
  value = data.aws_eks_cluster.scope.certificate_authority
}

output "cluster_arn" {
  value = data.aws_eks_cluster.scope.arn
}

output "cluster_logs" {
  value = data.aws_eks_cluster.scope.enabled_cluster_log_types
}

output "cluster_name" {
  value = data.aws_eks_cluster.scope.id
}

output "cluster_role" {
  value = data.aws_eks_cluster.scope.role_arn
}

output "cluster_tags" {
  value = data.aws_eks_cluster.scope.tags
}

output "cluster_status" {
  value = data.aws_eks_cluster.scope.status
}

output "cluster_created" {
  value = data.aws_eks_cluster.scope.created_at
}

output "cluster_release" {
  value = data.aws_eks_cluster.scope.version
}

output "cluster_endpoint" {
  value = data.aws_eks_cluster.scope.endpoint
}

output "cluster_identity" {
  value = data.aws_eks_cluster.scope.identity
}

output "cluster_platform" {
  value = data.aws_eks_cluster.scope.platform_version
}

output "cluster_vpc_config" {
  value = data.aws_eks_cluster.scope.vpc_config
}

output "cluster_zonal_config" {
  value = data.aws_eks_cluster.scope.zonal_shift_config
}

output "cluster_access_config" {
  value = data.aws_eks_cluster.scope.access_config
}

output "cluster_compute_config" {
  value = data.aws_eks_cluster.scope.compute_config
}

output "cluster_network_config" {
  value = data.aws_eks_cluster.scope.kubernetes_network_config
}

output "cluster_outpost_config" {
  value = data.aws_eks_cluster.scope.outpost_config
}

output "cluster_storage_config" {
  value = data.aws_eks_cluster.scope.storage_config
}

output "cluster_outpost_cluster" {
  value = data.aws_eks_cluster.scope.cluster_id
}

output "cluster_remote_network_config" {
  value = data.aws_eks_cluster.scope.remote_network_config
}

################################################################################

output "cluster_extended_support" {
  value = data.aws_eks_cluster.scope.upgrade_policy[0].support_type == "EXTENDED"
}

################################################################################

output "cluster_vpc_security_group_id" {
  value = data.aws_eks_cluster.scope.vpc_config[0].cluster_security_group_id
}

################################################################################
