################################################################################
# Optional Variables
################################################################################

variable "iam_role_attachment_ipv4_policy_name" {
  type        = string
  default     = "AmazonEKS_CNI_Policy"
  description = "The name of the built-in IAM policy to attach to the VPC CNI addon IAM role for IPv4 clusters."
}

variable "iam_role_attachment_ipv4_policy_path" {
  type        = string
  default     = ""
  description = "The path to the built-in IAM policy to attach to the VPC CNI addon IAM role for IPv4 clusters."

  validation {
    condition     = (var.iam_role_attachment_ipv4_policy_path == "") || endswith(var.iam_role_attachment_ipv4_policy_path, "/")
    error_message = "The path to the built-in IAM policy must be either the empty string or end with '/'."
  }
}

################################################################################
# Locals
################################################################################

locals {
  iam_role_attachment_policy_arn      = local.cluster_ipv6 ? aws_iam_policy.ipv6.arn : local.iam_role_attachment_ipv4_policy_arn
  iam_role_attachment_ipv4_policy_arn = format("arn:aws:iam::aws:policy/%s%s", var.iam_role_attachment_ipv4_policy_path, var.iam_role_attachment_ipv4_policy_name)
}

################################################################################
# Resources
################################################################################

resource "aws_iam_role_policy_attachment" "scope" {
  role       = aws_iam_role.scope.name
  policy_arn = local.iam_role_attachment_policy_arn
}

################################################################################
# Outputs
################################################################################

output "iam_role_attachment_ipv4_policy_arn" {
  value = local.iam_role_attachment_ipv4_policy_arn
}

output "iam_role_attachment_ipv4_policy_name" {
  value = var.iam_role_attachment_ipv4_policy_name
}

output "iam_role_attachment_ipv4_policy_path" {
  value = var.iam_role_attachment_ipv4_policy_path
}

################################################################################

output "iam_role_attachment_role" {
  value = aws_iam_role_policy_attachment.scope.role
}

output "iam_role_attachment_policy_arn" {
  value = aws_iam_role_policy_attachment.scope.policy_arn
}

################################################################################
