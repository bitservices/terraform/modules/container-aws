################################################################################
# Optional Variables
################################################################################

variable "iam_policy_ipv6_name" {
  type        = string
  default     = null
  description = "The full name of the VPC CNI addon IPv6 IAM policy."
}

variable "iam_policy_ipv6_path" {
  type        = string
  default     = "/"
  description = "The IAM path to the VPC CNI addon IPv6 IAM policy."
}

variable "iam_policy_ipv6_description" {
  type        = string
  default     = "Managed by Terraform"
  description = "The description of the VPC CNI addon IPv6 IAM policy."
}

################################################################################
# Locals
################################################################################

locals {
  iam_policy_ipv6_name = coalesce(var.iam_policy_ipv6_name, format("%s-%s-ipv6", var.cluster, var.name))
}

################################################################################
# Data Sources
################################################################################

data "aws_iam_policy_document" "iam_policy_ipv6" {
  statement {
    sid    = "AllowManageIPv6"
    effect = "Allow"

    actions = [
      "ec2:AssignIpv6Addresses",
      "ec2:DescribeInstances",
      "ec2:DescribeTags",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DescribeInstanceTypes"
    ]

    resources = [
      "*"
    ]
  }

  statement {
    sid    = "AllowCreateTags"
    effect = "Allow"

    actions = [
      "ec2:CreateTags"
    ]

    resources = [
      format("arn:aws:ec2:%s:%s:network-interface/*", data.aws_region.scope.name, data.aws_caller_identity.scope.account_id)
    ]
  }
}

################################################################################
# Resources
################################################################################

resource "aws_iam_policy" "ipv6" {
  name        = local.iam_policy_ipv6_name
  path        = var.iam_policy_ipv6_path
  policy      = data.aws_iam_policy_document.iam_policy_ipv6.json
  description = var.iam_policy_ipv6_description
}

################################################################################
# Outputs
################################################################################

output "iam_policy_ipv6_id" {
  value = aws_iam_policy.ipv6.id
}

output "iam_policy_ipv6_arn" {
  value = aws_iam_policy.ipv6.arn
}

output "iam_policy_ipv6_name" {
  value = aws_iam_policy.ipv6.name
}

output "iam_policy_ipv6_path" {
  value = aws_iam_policy.ipv6.path
}

output "iam_policy_ipv6_policy" {
  value = aws_iam_policy.ipv6.policy
}

output "iam_policy_ipv6_description" {
  value = aws_iam_policy.ipv6.description
}

################################################################################
