################################################################################
# Required Variables
################################################################################

variable "vpc" {
  type        = string
  description = "The name of the VPC to which this asset belongs."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################

variable "cluster" {
  type        = string
  description = "Name of the EKS Cluster."
}

################################################################################
# Optional Variables
################################################################################

variable "name" {
  type        = string
  default     = "vpc-cni"
  description = "Name of the VPC CNI addon for EKS."
}

variable "release" {
  type        = string
  default     = "v1.19.3-eksbuild.1"
  description = "Version of the VPC CNI addon for EKS."
}

################################################################################

variable "conflicts" {
  type        = string
  default     = "OVERWRITE"
  description = "Default conflict resolution strategy for the VPC CNI addon for EKS. Must be 'NONE' or 'OVERWRITE'."

  validation {
    condition     = contains(["NONE", "OVERWRITE"], var.conflicts)
    error_message = "Default conflict resolution strategy for the VPC CNI addon must be either 'NONE' or 'OVERWRITE'."
  }
}

variable "conflicts_create" {
  type        = string
  default     = null
  description = "Conflict resolution strategy for the creation of the VPC CNI addon for EKS. Defaults to 'conflicts'."

  validation {
    condition     = var.conflicts_create == null ? true : contains(["NONE", "OVERWRITE"], var.conflicts_create)
    error_message = "Default conflict resolution strategy for the creation of the VPC CNI addon must be either 'NONE' or 'OVERWRITE'."
  }
}

variable "conflicts_update" {
  type        = string
  default     = null
  description = "Conflict resolution strategy for the update of the VPC CNI addon for EKS. Defaults to 'conflicts'."

  validation {
    condition     = var.conflicts_update == null ? true : contains(["NONE", "OVERWRITE", "PRESERVE"], var.conflicts_update)
    error_message = "Default conflict resolution strategy for the update of the VPC CNI addon must be either 'NONE', 'OVERWRITE' or 'PRESERVE'."
  }
}

################################################################################

variable "resources_node_cpu" {
  type        = string
  default     = "20m"
  description = "The Kubernetes CPU resource request for each 'node' container that runs within this add-on. Use 'null' for add-on default."
}

variable "resources_node_memory" {
  type        = string
  default     = null
  description = "The Kubernetes memory resource limit for each 'node' container that runs within this add-on. Use 'null' for add-on default."
}

################################################################################

variable "resources_nodeagent_cpu" {
  type        = string
  default     = "5m"
  description = "The Kubernetes CPU resource request for each 'nodeagent' container that runs within this add-on. Use 'null' for add-on default."
}

variable "resources_nodeagent_memory" {
  type        = string
  default     = null
  description = "The Kubernetes memory resource limit for each 'nodeagent' container that runs within this add-on. Use 'null' for add-on default."
}

################################################################################
# Locals
################################################################################

locals {
  conflicts_create = coalesce(var.conflicts_create, var.conflicts)
  conflicts_update = coalesce(var.conflicts_update, var.conflicts)

  configuration_resources_node_cpu = var.resources_node_cpu != null ? tomap({
    "cpu" = var.resources_node_cpu
  }) : tomap({})

  configuration_resources_node_memory = var.resources_node_memory != null ? tomap({
    "memory" = var.resources_node_memory
  }) : tomap({})

  configuration_resources_nodeagent_cpu = var.resources_nodeagent_cpu != null ? tomap({
    "cpu" = var.resources_nodeagent_cpu
  }) : tomap({})

  configuration_resources_nodeagent_memory = var.resources_nodeagent_memory != null ? tomap({
    "memory" = var.resources_nodeagent_memory
  }) : tomap({})
}

################################################################################
# Resources
################################################################################

resource "aws_eks_addon" "scope" {
  addon_name                  = var.name
  cluster_name                = var.cluster
  addon_version               = var.release
  service_account_role_arn    = aws_iam_role.scope.arn
  resolve_conflicts_on_create = local.conflicts_create
  resolve_conflicts_on_update = local.conflicts_update

  tags = {
    "VPC"     = var.vpc
    "IPv6"    = local.cluster_ipv6 ? "Enabled" : "Disabled"
    "Name"    = var.name
    "Owner"   = var.owner
    "Region"  = data.aws_region.scope.name
    "Cluster" = var.cluster
    "Company" = var.company
  }

  configuration_values = jsonencode({
    "nodeAgent" = {
      "resources" = {
        "limits"   = local.configuration_resources_nodeagent_memory
        "requests" = merge(local.configuration_resources_nodeagent_cpu, local.configuration_resources_nodeagent_memory)
      }
    }

    "resources" = {
      "limits"   = local.configuration_resources_node_memory
      "requests" = merge(local.configuration_resources_node_cpu, local.configuration_resources_node_memory)
    }
  })

  depends_on = [
    aws_iam_role_policy_attachment.scope
  ]
}

################################################################################
# Outputs
################################################################################

output "vpc" {
  value = var.vpc
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "resources_node_cpu" {
  value = var.resources_node_cpu
}

output "resources_node_memory" {
  value = var.resources_node_memory
}

################################################################################

output "resources_nodeagent_cpu" {
  value = var.resources_nodeagent_cpu
}

output "resources_nodeagent_memory" {
  value = var.resources_nodeagent_memory
}

################################################################################

output "configuration_resources_node_cpu" {
  value = local.configuration_resources_node_cpu
}

output "configuration_resources_node_memory" {
  value = local.configuration_resources_node_memory
}

################################################################################

output "configuration_resources_nodeagent_cpu" {
  value = local.configuration_resources_nodeagent_cpu
}

output "configuration_resources_nodeagent_memory" {
  value = local.configuration_resources_nodeagent_memory
}

################################################################################

output "id" {
  value = aws_eks_addon.scope.id
}

output "arn" {
  value = aws_eks_addon.scope.arn
}

output "name" {
  value = aws_eks_addon.scope.addon_name
}

output "tags" {
  value = aws_eks_addon.scope.tags_all
}

output "cluster" {
  value = aws_eks_addon.scope.cluster_name
}

output "created" {
  value = aws_eks_addon.scope.created_at
}

output "release" {
  value = aws_eks_addon.scope.addon_version
}

output "modified" {
  value = aws_eks_addon.scope.modified_at
}

output "configuration" {
  value = aws_eks_addon.scope.configuration_values
}

output "conflicts_create" {
  value = aws_eks_addon.scope.resolve_conflicts_on_create
}

output "conflicts_update" {
  value = aws_eks_addon.scope.resolve_conflicts_on_update
}

################################################################################
