################################################################################
# Required Variables
################################################################################

variable "iam_role_kubernetes_oidc_arn" {
  type        = string
  description = "The Kubernetes OIDC ARN required for the AWS EFS CSI Driver addon role."
}

variable "iam_role_kubernetes_oidc_url" {
  type        = string
  description = "The Kubernetes OIDC URL required for the AWS EFS CSI Driver addon role."
}

################################################################################
# Optional Variables
################################################################################

variable "iam_role_name" {
  type        = string
  default     = null
  description = "The full name of the AWS EFS CSI Driver addon IAM role."
}

variable "iam_role_path" {
  type        = string
  default     = "/"
  description = "The IAM path to the AWS EFS CSI Driver addon role."
}

variable "iam_role_description" {
  type        = string
  default     = "Managed by Terraform"
  description = "The description of the AWS EFS CSI Driver addon IAM role."
}

variable "iam_role_force_detach" {
  type        = bool
  default     = false
  description = "Specifies to force detaching any policies the AWS EFS CSI Driver addon IAM role has before destroying it."
}

################################################################################

variable "iam_role_policy_namespace" {
  type        = string
  default     = "kube-system"
  description = "The namespace used by the AWS EFS CSI Driver addon. Required for building the AWS EFS CSI Driver addon IAM role assume policy."
}

variable "iam_role_policy_service_account_node" {
  type        = string
  default     = "efs-csi-node-sa"
  description = "The service account used by the AWS EFS CSI Driver addon node component. Required for building the AWS EFS CSI Driver addon IAM role assume policy."
}

variable "iam_role_policy_service_account_controller" {
  type        = string
  default     = "efs-csi-controller-sa"
  description = "The service account used by the AWS EFS CSI Driver addon controller component. Required for building the AWS EFS CSI Driver addon IAM role assume policy."
}

################################################################################
# Locals
################################################################################

locals {
  iam_role_name                = coalesce(var.iam_role_name, format("%s-%s", var.cluster, var.name))
  iam_role_value_node          = format("system:serviceaccount:%s:%s", var.iam_role_policy_namespace, var.iam_role_policy_service_account_node)
  iam_role_value_controller    = format("system:serviceaccount:%s:%s", var.iam_role_policy_namespace, var.iam_role_policy_service_account_controller)
  iam_role_kubernetes_oidc_sub = format("%s:sub", replace(var.iam_role_kubernetes_oidc_url, "https://", ""))
}

################################################################################
# Data Sources
################################################################################

data "aws_iam_policy_document" "iam_role_assume_policy" {
  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRoleWithWebIdentity"
    ]

    condition {
      test     = "StringEquals"
      variable = local.iam_role_kubernetes_oidc_sub

      values = [
        local.iam_role_value_node,
        local.iam_role_value_controller
      ]
    }

    principals {
      type = "Federated"

      identifiers = [
        var.iam_role_kubernetes_oidc_arn
      ]
    }
  }
}

################################################################################
# Resources
################################################################################

resource "aws_iam_role" "scope" {
  name                  = local.iam_role_name
  path                  = var.iam_role_path
  description           = var.iam_role_description
  assume_role_policy    = data.aws_iam_policy_document.iam_role_assume_policy.json
  force_detach_policies = var.iam_role_force_detach
}

################################################################################
# Outputs
################################################################################

output "iam_role_kubernetes_oidc_arn" {
  value = var.iam_role_kubernetes_oidc_arn
}

output "iam_role_kubernetes_oidc_sub" {
  value = local.iam_role_kubernetes_oidc_sub
}

output "iam_role_kubernetes_oidc_url" {
  value = var.iam_role_kubernetes_oidc_url
}

################################################################################

output "iam_role_policy_namespace" {
  value = var.iam_role_policy_namespace
}

output "iam_role_policy_service_account_node" {
  value = var.iam_role_policy_service_account_node
}

output "iam_role_policy_service_account_controller" {
  value = var.iam_role_policy_service_account_controller
}

################################################################################

output "iam_role_value_node" {
  value = local.iam_role_value_node
}

output "iam_role_value_controller" {
  value = local.iam_role_value_controller
}

################################################################################

output "iam_role_id" {
  value = aws_iam_role.scope.unique_id
}

output "iam_role_arn" {
  value = aws_iam_role.scope.arn
}

output "iam_role_name" {
  value = aws_iam_role.scope.name
}

output "iam_role_path" {
  value = aws_iam_role.scope.path
}

output "iam_role_created" {
  value = aws_iam_role.scope.create_date
}

output "iam_role_description" {
  value = aws_iam_role.scope.description
}

output "iam_role_force_detach" {
  value = aws_iam_role.scope.force_detach_policies
}

output "iam_role_assume_policy" {
  value = aws_iam_role.scope.assume_role_policy
}

################################################################################
