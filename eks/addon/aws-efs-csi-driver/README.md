<!----------------------------------------------------------------------------->

# eks/addon/aws-efs-csi-driver

#### Manage the [AWS EFS CSI Driver] Elastic [Kubernetes] Service ([EKS]) addon

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/container/aws//eks/addon/aws-efs-csi-driver`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "vpc"     { default = "sandpit01"                }
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_eks_cluster" {
  source  = "gitlab.com/bitservices/container/aws//eks/cluster"
  vpc     = var.vpc
  owner   = var.owner
  company = var.company
}

module "my_eks_node_group_default_a" {
  source      = "gitlab.com/bitservices/container/aws//eks/node-group"
  vpc         = var.vpc
  class       = "default-a"
  owner       = var.owner
  company     = var.company
  cluster     = module.my_eks_cluster.name
  release     = module.my_eks_cluster.release
  subnet_zone = "a"
}

module "my_eks_node_group_default_b" {
  source      = "gitlab.com/bitservices/container/aws//eks/node-group"
  vpc         = var.vpc
  class       = "default-b"
  owner       = var.owner
  company     = var.company
  cluster     = module.my_eks_cluster.name
  release     = module.my_eks_cluster.release
  subnet_zone = "b"
}

module "my_eks_addon_aws_efs_csi_driver" {
  source                       = "gitlab.com/bitservices/container/aws//eks/addon/aws-efs-csi-driver"
  vpc                          = var.vpc
  owner                        = var.owner
  company                      = var.company
  cluster                      = module.my_eks_cluster.name
  iam_role_kubernetes_oidc_arn = module.my_eks_cluster.iam_openid_connect_provider_arn
  iam_role_kubernetes_oidc_url = module.my_eks_cluster.iam_openid_connect_provider_url

  depends_on = [
    module.my_eks_node_group_default_a,
    module.my_eks_node_group_default_b
  ]
}

```

<!----------------------------------------------------------------------------->

[EKS]:                https://aws.amazon.com/eks/
[Kubernetes]:         https://kubernetes.io/
[AWS EFS CSI Driver]: https://docs.aws.amazon.com/eks/latest/userguide/efs-csi.html

<!----------------------------------------------------------------------------->
